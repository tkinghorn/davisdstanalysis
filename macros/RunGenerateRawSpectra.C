void RunGenerateRawSpectra(TString yieldHistoFile, TString outputSpectraFile, TString pidCalibrationFile,
			   TString starLibrary, Double_t energy, TString eventConfig, Int_t particleSpecies=-999,
			   Int_t centIndex=-999, Double_t rapidity=-999){

  gSystem->Load("../bin/TrackInfo_cxx.so");
  gSystem->Load("../bin/PrimaryVertexInfo_cxx.so");
  gSystem->Load("../bin/EventInfo_cxx.so");
  gSystem->Load("../bin/utilityFunctions_cxx.so");
  gSystem->Load("../bin/ParticleInfo_cxx.so");
  gSystem->Load("../bin/fitZTPCUtilities_cxx.so");
  gSystem->Load("../bin/StRefMultExtendedCorr_cxx.so");
  gSystem->Load("../bin/UserCuts_cxx.so");
  gSystem->Load("../bin/YieldExtractionFit_cxx.so");
  gSystem->Load("../bin/generateRawSpectra_cxx.so");
  
  //Find the rapidity index of the user specified rapidity value
  Int_t rapidityIndex = -1;
  if (rapidity != -999)
    rapidityIndex = GetRapidityIndex(rapidity);

  //Check to make sure the rapidityIndex is good before continuing
  if (rapidity != -999 && rapidityIndex < 0){
    cout <<"ERROR in RunFitZTPCPions.C: value of rapidity is not in rapidity range! EXITING" <<endl;
    return;
  }

  SetVariableUserCuts(energy,eventConfig,starLibrary);
  
  GenerateRawSpectra(yieldHistoFile,outputSpectraFile,pidCalibrationFile,particleSpecies,centIndex,rapidityIndex);

}
