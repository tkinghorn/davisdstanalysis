//This macro runs the compiled pileupChiSquaredMinimzation binary
void RunPileUpChiSquaredMinimization(TString fitFile="", TString dataFile="", TString glauberFile="", TString outputFile="",
												Int_t normStartBin, Int_t normEndBin, Int_t lastBin, Double_t NegativeBinomialParameterNPP, Double_t NegativeBinomialParameterK, 
												Int_t numberOfPercentages, Double_t percentBeg, Double_t percentEnd, Int_t nSimulatedEvents){

	//Load the necessary libraries
	gSystem->Load("../bin/GlauberClass_cxx.so");
	gSystem->Load("../bin/GlauberModel_cxx.so");  
	gSystem->Load("../bin/pileupChiSquaredMinimization_cxx.so");

	pileupChiSquaredMinimization(fitFile, dataFile, glauberFile, outputFile, normStartBin, normEndBin, lastBin, NegativeBinomialParameterNPP, NegativeBinomialParameterK, numberOfPercentages, percentBeg, percentEnd, nSimulatedEvents);

}
