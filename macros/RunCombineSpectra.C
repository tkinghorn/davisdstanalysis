//Runs the source code responsible for applying all of the corrections to the spectra
//see src/analysis/correctSpectra.cxx

void RunCombineSpectra(int pid, int charge, int midYIndex, TString resultsFile, TString fileCenter,
		       TString filePosY, TString fileNegY,
		       Double_t energy){
  
  //Load the necessary Librarires
  gSystem->Load("../bin/utilityFunctions_cxx.so");
  gSystem->Load("../bin/TrackInfo_cxx.so");
  gSystem->Load("../bin/PrimaryVertexInfo_cxx.so");
  gSystem->Load("../bin/EventInfo_cxx.so");
  gSystem->Load("../bin/DavisDstReader_cxx.so");
  gSystem->Load("../bin/StRefMultExtendedCorr_cxx.so");
  gSystem->Load("../bin/UserCuts_cxx.so");
  gSystem->Load("../bin/ParticleInfo_cxx.so");
  gSystem->Load("../bin/SpectraCorrectionCurves_cxx.so");
  gSystem->Load("../bin/SpectraCorrectionFunctions_cxx.so");
  gSystem->Load("../bin/SpectraClass_cxx.so");
  gSystem->Load("../bin/combineSpectra_cxx.so");

  //Note: It doesn't matter what the event config and star lib version are 
  SetVariableUserCuts(energy,"","");
  
  combineSpectra(pid,charge,midYIndex,resultsFile,fileCenter,filePosY,fileNegY);

}
