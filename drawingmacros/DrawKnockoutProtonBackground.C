#include <vector>

#include "../inc/globalDefinitions.h"
#include "../inc/StyleSettings.h"

#include <TF1.h>

void DrawKnockoutProtonBackground(TString eventConfig, TString system){

  gSystem->Load("../bin/utilityFunctions_cxx.so");
  gSystem->Load("../bin/TrackInfo_cxx.so");
  gSystem->Load("../bin/PrimaryVertexInfo_cxx.so");
  gSystem->Load("../bin/EventInfo_cxx.so");
  gSystem->Load("../bin/ParticleInfo_cxx.so");
  gSystem->Load("../bin/StRefMultExtendedCorr_cxx.so");
  gSystem->Load("../bin/UserCuts_cxx.so");
  gSystem->Load("../bin/SpectraFitFunctions_cxx.so");
  SetVariableUserCuts(7.7,eventConfig,"SL10h");
  ParticleInfo *particleInfo = new ParticleInfo();

  const int nEnergies(7);
  const int nCentBins(GetNCentralityBins());
  Double_t energies[nEnergies] = {7.7,11.5,14.5,19.6,27.0,39.0,62.4};
    
  //The Correction Files
  TFile *corrFile[nEnergies];
  corrFile[0] = new TFile(Form("../userfiles/AuAu07/outputFiles/AuAu07_%s_CorrectionFile.root",eventConfig.Data()),"READ");
  corrFile[1] = new TFile(Form("../userfiles/AuAu11/outputFiles/AuAu11_%s_CorrectionFile.root",eventConfig.Data()),"READ");
  corrFile[2] = new TFile(Form("../userfiles/AuAu14/outputFiles/AuAu14_%s_CorrectionFile.root",eventConfig.Data()),"READ");
  corrFile[3] = new TFile(Form("../userfiles/AuAu19/outputFiles/AuAu19_%s_CorrectionFile.root",eventConfig.Data()),"READ");
  corrFile[4] = new TFile(Form("../userfiles/AuAu27/outputFiles/AuAu27_%s_CorrectionFile.root",eventConfig.Data()),"READ");
  corrFile[5] = new TFile(Form("../userfiles/AuAu39/outputFiles/AuAu39_%s_CorrectionFile.root",eventConfig.Data()),"READ");
  corrFile[6] = new TFile(Form("../userfiles/AuAu62/outputFiles/AuAu62_%s_CorrectionFile.root",eventConfig.Data()),"READ");

  //The Knockout Curves
  TF1 *knockoutCurve[nEnergies][nCentBins][nRapidityBins];
  for (int iEnergy=0; iEnergy<nEnergies; iEnergy++){
    for (int iCentBin=0; iCentBin<nCentBins; iCentBin++){
      for (int yIndex=GetMinRapidityIndexOfInterest(PROTON); yIndex<=GetMaxRapidityIndexOfInterest(PROTON); yIndex++){
	knockoutCurve[iEnergy][iCentBin][yIndex] = NULL;
	knockoutCurve[iEnergy][iCentBin][yIndex] =
	  (TF1 *)corrFile[iEnergy]->Get(Form("ProtonPlus/KnockoutBackgroundFits/knockoutProton_%d_%d_Fit;2",
					     iCentBin,yIndex));
	if (!knockoutCurve[iEnergy][iCentBin][yIndex])
	  cout <<"WARNING: Knockout Curve does not exist! " <<iEnergy <<" " <<iCentBin <<" " <<yIndex <<endl;
	knockoutCurve[iEnergy][iCentBin][yIndex]->SetNpx(10000);
      }
    }
  }

  int yIndex=20;
  TCanvas *canvas[nEnergies];
  TPaveText *title[nEnergies];
  TLegend *leg[nEnergies];
  for (int iEnergy=0; iEnergy<nEnergies; iEnergy++){

    canvas[iEnergy] = new TCanvas(Form("canvas_%d",iEnergy),"",20,20,400,400);
    canvas[iEnergy]->SetTopMargin(.05);
    canvas[iEnergy]->SetRightMargin(.05);
    canvas[iEnergy]->SetTicks();
    TH1F *frame = canvas[iEnergy]->DrawFrame(0.075,-.025,.3,.15);
    frame->GetXaxis()->SetTitleFont(63);
    frame->GetXaxis()->SetTitleSize(15);
    frame->GetXaxis()->SetTitle("m_{T}-m_{0} (GeV/c^{2})");
   

    title[iEnergy] = new TPaveText(.60,.70,.90,.90,"brNDC");
    title[iEnergy]->SetFillColor(kWhite);
    title[iEnergy]->SetBorderSize(0);
    title[iEnergy]->SetTextAlign(31);
    title[iEnergy]->SetTextFont(63);
    title[iEnergy]->SetTextSize(20);
    title[iEnergy]->AddText("Knockout Proton Fraction");
    title[iEnergy]->AddText(Form("%s #sqrt{s_{NN}}=%.03g GeV",system.Data(),energies[iEnergy]));
    title[iEnergy]->AddText(Form("y_{p}=[%.03g,%.03g]",				 
    				 GetRapidityRangeLow(yIndex),GetRapidityRangeHigh(yIndex)));
    title[iEnergy]->Draw("SAME");

    leg[iEnergy] = new TLegend(.57,.36,.91,.68);
    leg[iEnergy]->SetNColumns(2);
    leg[iEnergy]->SetFillColor(kWhite);
    leg[iEnergy]->SetBorderSize(0);
    leg[iEnergy]->SetTextFont(63);
    leg[iEnergy]->SetTextSize(15);

    for (int iCentBin=8; iCentBin>=0; iCentBin--){
      knockoutCurve[iEnergy][iCentBin][yIndex]->SetLineWidth(2);
      knockoutCurve[iEnergy][iCentBin][yIndex]->SetLineColor(GetCentralityColor(iCentBin));
      knockoutCurve[iEnergy][iCentBin][yIndex]->Draw("SAME");

      int iCentHigh = (int)GetCentralityPercents().at(iCentBin);
      int iCentLow(0);
      if (iCentBin == nCentBins-1)
	iCentLow = 0;
      else
	iCentLow = (int)GetCentralityPercents().at(iCentBin+1);
      
      leg[iEnergy]->AddEntry(knockoutCurve[iEnergy][iCentBin][yIndex],Form("%d-%d%%",iCentLow,iCentHigh),"L");

    }
    leg[iEnergy]->Draw("SAME");

    canvas[iEnergy]->Update();
    //canvas[iEnergy]->SaveAs(Form("%s.png",canvas[iEnergy]->GetName()));
  }



}
