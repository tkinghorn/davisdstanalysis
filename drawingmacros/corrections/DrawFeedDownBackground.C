#include "../../inc/globalDefinitions.h"
#include "../../inc/StyleSettings.h"
#include "../../inc/DrawingFunctions.h"


//___________________________________________________________________________
void DrawSingleFeedDownPlot(TFile *file, TPad *pad, Double_t energy, Int_t pid, Int_t charge,
			     Int_t centBin, Int_t yIndex){


  ParticleInfo particleInfo;

  //Get the Graph from the file
  TGraphErrors *graph =
    (TGraphErrors *)file->Get(Form("%s/FeedDownBackgroundGraphs/feeddownBackground_%s_cent%02d_yIndex%02d",
				   particleInfo.GetParticleName(pid,charge).Data(),
				   particleInfo.GetParticleName(pid,charge).Data(),
				   centBin,yIndex));
  //If the Graph Does not exist then return a canvas saying so
  if (!graph){
    
    TPaveText *noGraph = new TPaveText(.1,.1,.9,.9,"NDC");
    noGraph->SetFillColor(kWhite);
    noGraph->SetBorderSize(0);
    noGraph->SetTextFont(62);
    noGraph->AddText("No Graph Available.");
    noGraph->Draw();
    return;
  }
  graph->SetMarkerSize(1.2);
  graph->SetMarkerColor(particleInfo.GetParticleColor(pid));
  TGraphErrors *fitSys =
    (TGraphErrors *)file->Get(Form("%s/FeedDownBackgroundFits/feeddownBackground_%s_cent%02d_yIndex%02d_FitExp_Conf",
				   particleInfo.GetParticleName(pid,charge).Data(),
				   particleInfo.GetParticleName(pid,charge).Data(),
				   centBin,yIndex));
  
  TGraphErrors *fitSys1 =
    (TGraphErrors *)file->Get(Form("%s/FeedDownBackgroundFits/feeddownBackground_%s_cent%02d_yIndex%02d_FitPower_Conf",
				   particleInfo.GetParticleName(pid,charge).Data(),
				   particleInfo.GetParticleName(pid,charge).Data(),
				   centBin,yIndex));

  Double_t yMax = 1.0;
  if (pid == PION)
    yMax = .15;
  else if (pid == KAON)
    yMax = .1;
  else if (pid == PROTON)
    yMax = .4;

  TString yAxisTitle = "Feed Down Fraction";
  if (pid == PROTON && charge > 0)
    yAxisTitle = "Feed Down + Knockout Fraction";
  
  TH1F *frame = MakeFrame(pad,yMax);
  pad->SetLeftMargin(.12);
  frame->SetTitle(Form(";(m_{T}-m_{%s})^{Reco} (GeV/c^{2});%s",
		       particleInfo.GetParticleSymbol(pid).Data(),
		       yAxisTitle.Data()));
  frame->GetYaxis()->SetTitleOffset(1.35);
  TPaveText *title = MakeTitle(graph,energy,.5,.71,.95,.92);
  if (pid == PROTON && charge > 0){
    title->SetX1(.25);
  }
  
  title->Draw();

  fitSys1->Draw("3");
  fitSys->Draw("3");
  graph->Draw("PZ");
  
  TLegend *leg = MakeLegend(.63,.49,.95,.72);
  TF1 *fit = (TF1*)graph->GetListOfFunctions()->At(0);
  TF1 *fit1 = (TF1*)graph->GetListOfFunctions()->At(1);
  if (pid != PION)
    leg->AddEntry(fit,Form("#splitline{%.03ge^{%.03gx}}{#scale[0.8]{#chi^{2}/NDF = %.03g/%d}}",
			   fit->GetParameter(0),
			   fit->GetParameter(1),
			   fit->GetChisquare(),
			   fit->GetNDF()),"L");
  else
    leg->AddEntry(fit,Form("#splitline{%.03g+%.03ge^{%.03gx}}{#scale[0.8]{#chi^{2}/NDF = %.03g/%d}}",
			   fit->GetParameter(2),
			   fit->GetParameter(0),
			   fit->GetParameter(1),
			   fit->GetChisquare(),
			   fit->GetNDF()),"L");
  
  leg->AddEntry(fit1,Form("#splitline{%.03gx^{%.03g}}{#scale[0.8]{#chi^{2}/NDF = %.03g/%d}}",
			  fit1->GetParameter(0),
			  fit1->GetParameter(1),
			  fit1->GetChisquare(),
			  fit1->GetNDF()),"L");
  leg->Draw();
  
  
}

//___________________________________________________________________________
void DrawFeedDownBackground(TString inFile, Double_t energy, Int_t pid, Int_t charge, TString eventConfig,
			    TString saveDir="", Int_t centBin=-1, Int_t yBin=-1){

  
  gROOT->LoadMacro("../../macros/loadLibs.C");
  gROOT->LoadMacro("DrawMuonContamination.C"); //For the Title, legend, and frame functions
  LoadParticleInfoModule();
  LoadUserCutsModule();
  SetVariableUserCuts(energy,eventConfig,"");

  const Int_t nCentBins = GetNCentralityBins();
  TFile *file = new TFile(inFile,"READ");
  ParticleInfo particleInfo;

  TCanvas *canvas = new TCanvas("canvas","canvas",20,20,800,600);
  
  //If Both a centrality bin and rapidity bin are specified then just do that bin
  if (centBin > 0 && yBin > 0){

    DrawSingleFeedDownPlot(file,gPad,energy,pid,charge,centBin,yBin);
    if (saveDir.CompareTo("")){
      canvas->Print(Form("%s/AuAu%02d_%s_%s_cent%02d_yIndex%02d_FeedDown.pdf",
			 saveDir.Data(),
			 TMath::Nint(energy),
			 eventConfig.Data(),
			 particleInfo.GetParticleName(pid,charge).Data(),
			 centBin,yBin),"pdf");
    }//End if Save
    return;
  }//End if just one bin

  //Loop Over the Centrality and Rapidity Bins for this energy and event Confifuration
  for (int iCentBin=0; iCentBin<nCentBins; iCentBin++){
    
    //If a particular centrality bin has been specified then skip the others
    if (centBin > 0 && iCentBin != centBin)
      continue;
    
    for (int yIndex=GetMinRapidityIndexOfInterest(pid); yIndex<=GetMaxRapidityIndexOfInterest(pid); yIndex++){

      //If a particular rapidity bin has been specified then skip the others
      if (yBin > 0 && yIndex != yBin)
	continue;

      TCanvas *canvas = DrawSingleFeedDownPlot(file,energy,pid,charge,iCentBin,yIndex);
      if (saveDir.CompareTo("")){
	canvas->Print(Form("%s/AuAu%02d_%s_%s_cent%02d_yIndex%02d_FeedDown.pdf",
			   saveDir.Data(),
			   (int)energy,
			   eventConfig.Data(),
			   particleInfo.GetParticleName(pid,charge).Data(),
			   iCentBin,yIndex),"pdf");

	delete canvas;
	
      }//End If Save

    }//End Loop Over yIndex
    
  }//End Loop Over CentBin
  
  
}
