#include "../../inc/globalDefinitions.h"
#include "../../inc/StyleSettings.h"
#include "../../inc/DrawingFunctions.h"

void DrawSingleEfficiencyPlot(TFile *file, TPad *pad, Double_t energy, int pid, int charge,
			      int centBin, int yIndex){

  pad->cd();
  ParticleInfo particleInfo;

  TGraphErrors *effGraph =
    (TGraphErrors *) file->Get(Form("%s/EfficiencyGraphs/tpcEfficiencyGraph_%s_Cent%d_yIndex%d",
				    particleInfo.GetParticleName(pid,charge).Data(),
				    particleInfo.GetParticleName(pid,charge).Data(),
				    centBin,yIndex));

  if (!effGraph){
    TPaveText *noGraph = new TPaveText(.1,.1,.9,.9,"NDC");
    noGraph->SetFillColor(kWhite);
    noGraph->SetBorderSize(0);
    noGraph->SetTextFont(62);
    noGraph->AddText("No Graph Available.");
    noGraph->Draw();
    return;
  }

  effGraph->GetListOfFunctions()->Last()->Delete();
  TF1 *fit = (TF1 *)effGraph->GetListOfFunctions()->First();
  fit->SetLineColor(kBlack);

  TH1F *frame = MakeFrame(pad,1.05,2.1);
  pad->SetLeftMargin(.11);
  frame->SetTitle(Form(";%s (GeV/c^{2});%s",
		       effGraph->GetXaxis()->GetTitle(),
		       effGraph->GetYaxis()->GetTitle()));
  effGraph->Draw("PZ");

  TPaveText *title = MakeTitle(effGraph,energy,.5,.25,.95,.47);
  title->Draw("SAME");

  TLegend *leg = new TLegend(.57,.14,.95,.28);
  leg->SetFillColor(kWhite);
  leg->SetBorderSize(0);
  leg->SetTextFont(62);
  leg->SetTextSize(.045);
  leg->SetTextAlign(12);
  leg->AddEntry(fit,
		    Form("#splitline{%.03fe^{-%f/x^{%.03f}}}{#scale[.8]{#chi^{2}/NDF = %.03f / %02d}}",
			 fit->GetParameter(0),
			 fit->GetParameter(1),
			 fit->GetParameter(2),
			 fit->GetChisquare(),fit->GetNDF()),"L");
  leg->Draw();
  pad->IsModified();
  pad->Update();

}

//_______________________________________________________
void DrawEfficiencyCorrection(){

  gStyle->SetOptTitle(0);

  gROOT->LoadMacro("../../macros/loadLibs.C");
  LoadParticleInfoModule();

  //Load the File
  TFile *file = new TFile("/home/chris/Documents/davisdstanalysis/userfiles/AuAu07/outputFiles/AuAu07_ColliderCenter_CorrectionFile.root");

  //Load the Efficiency Graphs
  int pid = 0;
  double energy = 7.7;
  int charge = 1;
  int centIndex = 8;
  int yIndex = 20;

  TCanvas *canvas = new TCanvas("canvas","canvas",20,20,500,1150);
  canvas->Divide(1,3);

  canvas->cd(1);
  DrawSingleEfficiencyPlot(file,gPad,energy,0,charge,centIndex,yIndex);

  canvas->cd(2);
  DrawSingleEfficiencyPlot(file,gPad,energy,1,charge,centIndex,yIndex);
  
  canvas->cd(3);
  DrawSingleEfficiencyPlot(file,gPad,energy,2,charge,centIndex,yIndex);

  canvas->IsModified();
  canvas->Update();
  
  return;

}
