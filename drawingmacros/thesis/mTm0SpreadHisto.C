{
//=========Macro generated from canvas: canvas1/canvas1
//=========  (Tue Feb  7 00:01:13 2017) by ROOT version5.34/36

  gStyle->SetOptStat(0);
  
  TCanvas *canvas1 = new TCanvas("canvas1", "canvas1",21,46,800,600);
   canvas1->Range(0,0,1,1);
   canvas1->SetFillColor(0);
   canvas1->SetBorderMode(0);
   canvas1->SetBorderSize(2);
   canvas1->SetFrameBorderMode(0);
   canvas1->SetTicks();
   canvas1->SetTopMargin(.05);
   canvas1->SetRightMargin(.07);
   
   TH1F *mTm0SpreadHisto = new TH1F("mTm0SpreadHisto","",100,0,0.0002);
   mTm0SpreadHisto->SetBinContent(1,39);
   mTm0SpreadHisto->SetBinContent(2,30);
   mTm0SpreadHisto->SetBinContent(3,41);
   mTm0SpreadHisto->SetBinContent(4,36);
   mTm0SpreadHisto->SetBinContent(5,38);
   mTm0SpreadHisto->SetBinContent(6,47);
   mTm0SpreadHisto->SetBinContent(7,32);
   mTm0SpreadHisto->SetBinContent(8,39);
   mTm0SpreadHisto->SetBinContent(9,30);
   mTm0SpreadHisto->SetBinContent(10,38);
   mTm0SpreadHisto->SetBinContent(11,32);
   mTm0SpreadHisto->SetBinContent(12,27);
   mTm0SpreadHisto->SetBinContent(13,26);
   mTm0SpreadHisto->SetBinContent(14,28);
   mTm0SpreadHisto->SetBinContent(15,26);
   mTm0SpreadHisto->SetBinContent(16,25);
   mTm0SpreadHisto->SetBinContent(17,32);
   mTm0SpreadHisto->SetBinContent(18,25);
   mTm0SpreadHisto->SetBinContent(19,21);
   mTm0SpreadHisto->SetBinContent(20,31);
   mTm0SpreadHisto->SetBinContent(21,12);
   mTm0SpreadHisto->SetBinContent(22,10);
   mTm0SpreadHisto->SetBinContent(23,31);
   mTm0SpreadHisto->SetBinContent(24,14);
   mTm0SpreadHisto->SetBinContent(25,20);
   mTm0SpreadHisto->SetBinContent(26,18);
   mTm0SpreadHisto->SetBinContent(27,19);
   mTm0SpreadHisto->SetBinContent(28,28);
   mTm0SpreadHisto->SetBinContent(29,13);
   mTm0SpreadHisto->SetBinContent(30,19);
   mTm0SpreadHisto->SetBinContent(31,12);
   mTm0SpreadHisto->SetBinContent(32,8);
   mTm0SpreadHisto->SetBinContent(33,9);
   mTm0SpreadHisto->SetBinContent(34,12);
   mTm0SpreadHisto->SetBinContent(35,11);
   mTm0SpreadHisto->SetBinContent(36,7);
   mTm0SpreadHisto->SetBinContent(37,7);
   mTm0SpreadHisto->SetBinContent(38,9);
   mTm0SpreadHisto->SetBinContent(39,3);
   mTm0SpreadHisto->SetBinContent(40,7);
   mTm0SpreadHisto->SetBinContent(41,7);
   mTm0SpreadHisto->SetBinContent(42,9);
   mTm0SpreadHisto->SetBinContent(43,6);
   mTm0SpreadHisto->SetBinContent(44,5);
   mTm0SpreadHisto->SetBinContent(45,4);
   mTm0SpreadHisto->SetBinContent(46,6);
   mTm0SpreadHisto->SetBinContent(47,7);
   mTm0SpreadHisto->SetBinContent(48,4);
   mTm0SpreadHisto->SetBinContent(49,6);
   mTm0SpreadHisto->SetBinContent(50,4);
   mTm0SpreadHisto->SetBinContent(51,4);
   mTm0SpreadHisto->SetBinContent(52,3);
   mTm0SpreadHisto->SetBinContent(53,3);
   mTm0SpreadHisto->SetBinContent(54,3);
   mTm0SpreadHisto->SetBinContent(55,3);
   mTm0SpreadHisto->SetBinContent(56,2);
   mTm0SpreadHisto->SetBinContent(57,1);
   mTm0SpreadHisto->SetBinContent(58,2);
   mTm0SpreadHisto->SetBinContent(59,1);
   mTm0SpreadHisto->SetBinContent(60,1);
   mTm0SpreadHisto->SetBinContent(63,1);
   mTm0SpreadHisto->SetBinContent(64,1);
   mTm0SpreadHisto->SetBinContent(67,1);
   mTm0SpreadHisto->SetBinContent(71,1);
   mTm0SpreadHisto->SetBinContent(73,2);
   mTm0SpreadHisto->SetBinContent(97,1);
   mTm0SpreadHisto->SetEntries(1000);

   gSystem->Load("../bin/ParticleInfo_cxx.so");
   ParticleInfo particleInfo;
   int pid = PION;
   int charge = -1;
   
   mTm0SpreadHisto->SetMarkerStyle(kFullCircle);
   mTm0SpreadHisto->SetMarkerColor(particleInfo.GetParticleColor(pid));
   mTm0SpreadHisto->SetLineColor(kBlack);
   mTm0SpreadHisto->SetMarkerSize(1.2);

   mTm0SpreadHisto->GetXaxis()->SetLabelFont(42);
   mTm0SpreadHisto->GetXaxis()->SetLabelSize(0.038);
   mTm0SpreadHisto->GetXaxis()->SetTitleSize(25);
   mTm0SpreadHisto->GetXaxis()->SetTitleFont(63);
   mTm0SpreadHisto->GetXaxis()->SetTitleOffset(1.05);
   mTm0SpreadHisto->GetXaxis()->SetTitle(Form("|(m_{T}-m_{%s}) - #LT m_{T}-m_{%s}#GT_{Inter-Bin}|",
					      particleInfo.GetParticleSymbol(pid,charge).Data(),
					      particleInfo.GetParticleSymbol(pid,charge).Data()));

   mTm0SpreadHisto->GetYaxis()->SetLabelFont(42);
   mTm0SpreadHisto->GetYaxis()->SetLabelSize(0.038);
   mTm0SpreadHisto->GetYaxis()->SetTitleSize(25);
   mTm0SpreadHisto->GetYaxis()->SetTitleFont(63);
   mTm0SpreadHisto->GetYaxis()->SetTitleOffset(.90);
   mTm0SpreadHisto->GetYaxis()->SetTitle("Counts");
   
    
   mTm0SpreadHisto->Draw("E");
   canvas1->Modified();
   canvas1->Update();


   TPaveText *title = new TPaveText(.40,.69,.90,.91,"NDC");
   title->SetFillColor(kWhite);
   title->SetBorderSize(0);
   title->SetTextFont(63);
   title->SetTextSize(25);
   title->SetTextAlign(11);
   title->AddText("Transverse Mass Systematic Error");
   title->AddText("#sqrt{s_{NN}} = 7.7 GeV | PID: #pi^{-}");
   title->AddText("Cent=[0,5]% | y_{#pi^{-}} = [-0.05,0.05]");
   title->AddText("m_{T}-m_{#pi^{-}} = [0.100,0.125] (GeV/c^{2})");
   title->GetLine(1)->SetTextSize(20);
   title->GetLine(2)->SetTextSize(20);
   title->GetLine(3)->SetTextSize(20);
   title->Draw();

   Double_t mean = mTm0SpreadHisto->GetMean();
   TLine *line = new TLine(mean,0,mean,40);
   line->SetLineColor(kBlack);
   line->SetLineStyle(9);
   line->SetLineWidth(3);
   line->Draw();
   
   TLegend *leg = new TLegend(.61,.58,.88,.68);
   leg->SetBorderSize(1);
   leg->SetFillColor(kWhite);
   leg->SetTextFont(63);
   leg->SetTextSize(20);
   leg->SetHeader(Form("Entries: %.05g",mTm0SpreadHisto->GetEntries()));
   leg->AddEntry(line,Form("Mean: %.03g",mean),"L");
   leg->Draw();
   
  
   canvas1->Modified();
   canvas1->cd();
   canvas1->SetSelected(canvas1);
}
