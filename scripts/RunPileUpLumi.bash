#!/bin/bash

#best fit histogram directory found from RunCentralityDetermination.sh
fitDirectory=../userfiles/AlAu_4_9GeV_2015/centralityBins/

# UNNORMALIZED data sorted by run number with included cuts in UserCuts.cxx
dataFile=../userfiles/AlAu_4_9GeV_2015/centralityVariableRuns/CentralityVariableDistributions.root

#glauber directory
glauberFile=../userfiles/AlAu_4_9GeV_2015/Glauber_27_197_30mb_WoodsSaxon.root

#output directory
outputDirectory=../userfiles/AlAu_4_9GeV_2015/pileupLumi/

energy=4.9

eventConfig="FixedTarget2015"

starLibrary="SL16a"

#region where to normalize. *Should* match the bins used in RunCentralityDetermination.sh
normStartBin=50
normEndBin=120

#Negative Binomial Parameters
npp=0.400991
k=0.751261

#Simulate how often a pileup event occurs between 0 and 1
pileUpEventOccurence=0.02

#number of simulated events
nSimulatedEvents=100000

#Draw canvas? Also includes two more histograms saved (not needed)
drawCanvas=false


#arrays
fitFiles=($fitDirectory/CentralityBins.root)
processID=()
numberOfFiles=${#fitFiles[@]}
outFiles=()

for i in ${fitFiles[@]}
do
	echo "Running on file: " $i

	outFile=$(basename $i .root)
	outFile=$outputDirectory/"$outFile"_pileupLumi.root

	outFiles+=($outFile)

	root -l -q -b ../macros/RunPileUpLuminosity.C\(\"$i\",\"$dataFile\",\"$glauberFile\",\"$outFile\",$energy,\"$eventConfig\",\"$starLibrary\",$normStartBin,$normEndBin,$npp,$k,$pileUpEventOccurence,$nSimulatedEvents,$drawCanvas\) #> /dev/null 2>&1 &

	processID+=($!)
	echo ${processID[@]}
done
wait ${processID[@]}

exit

