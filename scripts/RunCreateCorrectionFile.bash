#!/bin/bash                                                                                                          

help_func(){

    echo ""
    echo "HELP INFORMATION"
    echo "    NAME:"
    echo "        RunCreateCorrectionFile.bash"
    echo "    PURPOSE:"
    echo "        This runs the various scritps associated with making all of the"
    echo "        histograms and fits needed to constructed the correction file."
    echo "        Note that it can be run with options that prevent it from looping"
    echo "        over all of the miniMC.root files in case that is not necessary."
    echo "    USAGE:"
    echo "        ./RunCreateCorrectionFile.bash [OPTION] ... [CONFIG FILE]"
    echo "    REQUIRED ARGUMENTS:"
    echo "        configuration file - name of configuration file with path if applicable"
    echo "    OPTIONS:"
    echo "        -h - display this help information"
    echo "        -a - ANALYSIS ONLY - (1 = do not recreate histograms by rereading the minimc files)"
    echo ""
    echo ""

}

analysisOnly=0

#Check for options
while [ "$#" -gt 0 ]; do
    while getopts "a:h" opts; do
        case "$opts" in
	    a) analysisOnly="${OPTARG}"; shift;;
            h) help_func; exit 1;;
            ?) exit 1;;
            *) echo "For help use option: -h"; exit 1;;
        esac
        shift
        OPTIND=1
    done

    if [ "$#" -gt 0 ]; then
        POSITIONALPARAM=(${POSITIONALPARAM[@]} $1)
        shift
        OPTIND=1
    fi
done

#Make sure only one argument remains after all the options
if [ "${#POSITIONALPARAM[@]}" -ne 1 ]; then
    echo "ERROR: This script requires only one argument. For usage information use option -h."
    echo "       If you have used options check your formating."
    exit 1
fi

#Assign user inputs to variables
configFile=${POSITIONALPARAM[0]}

#Make Sure the configuration variable is set
if [ -z $configFile ]; then
    echo "ERROR: Configuration file was not set. For usage information use option -h"
    exit 1
fi

#Make Sure the configuration file exists
if [ ! -e $configFile ]; then
    echo "ERROR: Configruation file ($configFile) was not found!"
    exit 1
fi

#Get the Configuration from the Config file
source $configFile


#Pions
echo "Running RunMuonBackgroundAnalysis.bash"
./RunMuonBackgroundAnalysis.bash $configFile

#Kaons
#Nothing Special Here

#Protons
# It was determined that the knockout background should be computed
# using simulation results so it is incoorporrated into the feed down study below.

#All Particles
echo "Running RunMakeEmbeddingCorrectionCurves.bash"
./RunMakeEmbeddingCorrectionCurves.bash $configFile

echo "Running RunFeeddownBackgroundAnalysis.bash"
./RunFeeddownBackgroundAnalysis.bash $configFile

if [ $analysisOnly -eq 0 ]; then
    echo "Running RunInterBinmTm0Distributions.bash"
    ./RunInterBinmTm0Distributions.bash $configFile
else
    echo "Running RunInterBinmTm0Distributions.bash in ANALYSIS ONLY MODE"
    ./RunInterBinmTm0Distributions.bash -a $analysisOnly $configFile
fi

exit 0
    
