#include <iostream>
#include <vector>

#include <TFile.h>
#include <TMath.h>
#include <TF1.h>
#include <TString.h>
#include <TTree.h>
#include <TGraphErrors.h>
#include <TH1D.h>
#include <TH3D.h>
#include <TCanvas.h>
#include <TSystem.h>
#include <TStyle.h>
#include <TVirtualFitter.h>

#include "globalDefinitions.h"
#include "ParticleInfo.h"
#include "UserCuts.h"

#include "utilityFunctions.h"
#include "fitZTPCUtilities.h"

//Choose to draw or not
bool draw=false;

//______MAIN________________________________________________
void ExtractMeansAndWidths(TString inputYieldHistoFile, TString pidCalibrationFile){

  gStyle->SetOptFit(1);
  
  //Create an instance of Particle Info
  ParticleInfo *particleInfo = new ParticleInfo(GetStarLibraryVersion(),true,nRapidityBins,pidCalibrationFile);

  //Create new Directory in the pidCalibration File
  TFile *pidFile = new TFile(pidCalibrationFile,"UPDATE");
  pidFile->mkdir("MeanAndWidthGraphs");
  pidFile->mkdir("MeanAndWidthFits");
  
  //Open the input Yield Histogram file
  TFile *yieldHistoFile = new TFile(inputYieldHistoFile,"READ");
  
  //Create a Canvas
  TCanvas *canvas = NULL;
  if (draw){
    canvas = new TCanvas("canvas","canvas",20,20,1800,1100);
    canvas->Divide(3,2);
    canvas->cd(3);
    gPad->SetLogy();
    canvas->cd(6);
    gPad->SetLogy();
    
  }
  
  //Particles Whose means and widths should be extracted
  std::vector<int> particles;
  particles.push_back(PION);
  particles.push_back(KAON);
  particles.push_back(PROTON);

  const unsigned int nParticles = particles.size();
  const unsigned int nCentralityBins = GetNCentralityBins();

  //Graphs to hold the means and widths
  std::vector<std::vector<std::vector<TGraphErrors *> > >
    meanGraph (nParticles, std::vector<std::vector<TGraphErrors *> >
	       (nParticles, std::vector<TGraphErrors *>
		(nRapidityBins, (TGraphErrors *) NULL)));
  std::vector<std::vector<std::vector<TGraphErrors *> > >
    widthGraph (nParticles, std::vector<std::vector<TGraphErrors *> >
		(nParticles, std::vector<TGraphErrors *>
		 (nRapidityBins, (TGraphErrors *) NULL)));
  std::vector<std::vector<std::vector<TGraphErrors *> > >
    meanGraphTOF (nParticles, std::vector<std::vector<TGraphErrors *> >
		  (nParticles, std::vector<TGraphErrors *>
		   (nRapidityBins, (TGraphErrors *) NULL)));
  std::vector<std::vector<std::vector<TGraphErrors *> > >
    widthGraphTOF (nParticles, std::vector<std::vector<TGraphErrors *> >
		   (nParticles, std::vector<TGraphErrors *>
		    (nRapidityBins, (TGraphErrors *) NULL)));

  //The Tof Optimized Histograms
  yieldHistoFile->cd("YieldHistograms");
  std::vector<std::vector<TH3D *> > tofOptZTPCHisto3D (nParticles, std::vector<TH3D *>
						       (nParticles,(TH3D *) NULL));
  std::vector<TH3D *> zTOFHisto3D (nParticles, (TH3D *)NULL);
  for (unsigned int iParticle=0; iParticle<nParticles; iParticle++){
    for (unsigned int iTofOptIndex=0; iTofOptIndex<nParticles; iTofOptIndex++){
      tofOptZTPCHisto3D.at(iParticle).at(iTofOptIndex) = (TH3D *)gDirectory->
	Get(Form("zTPC_%s_%s",particleInfo->GetParticleName(particles.at(iParticle)).Data(),
		 particleInfo->GetParticleName(particles.at(iTofOptIndex)).Data()));
    }//End Loop Over Tof Opt Particles
    zTOFHisto3D.at(iParticle) = (TH3D *)gDirectory->
      Get(Form("zTOF_AllCentCharge_%s",particleInfo->GetParticleName(particles.at(iParticle)).Data()));
  }//End Loop Over Particles
  
  //Loop Over the Particles and Fit the tof optimized histograms to extract
  //the means and widths and fill the graphs with them.
  TH1D *currentHisto = NULL, *currentHistoTOF = NULL;
  TGraphErrors *currentMeanGraph, *currentWidthGraph;
  TGraphErrors *currentMeanGraphTOF, *currentWidthGraphTOF;
  for (unsigned int iParticle=0; iParticle<nParticles; iParticle++){
    int particleSpecies = particles.at(iParticle);

    std::vector<std::vector<TH1D *> > tofOptZTPCHistos (nRapidityBins, std::vector<TH1D *>
							(nmTm0Bins, (TH1D *) NULL));
    std::vector<std::vector<TH1D *> > zTOFHistos (nRapidityBins, std::vector<TH1D *>
						  (nmTm0Bins, (TH1D *) NULL));

    //Create All The Graphs
    for (unsigned int iSubSpecies=0; iSubSpecies<nParticles; iSubSpecies++){
      int subSpecies = particles.at(iSubSpecies);

      for (unsigned int yIndex=0; yIndex<nRapidityBins; yIndex++){
	Double_t rapidity = GetRapidityRangeCenter(yIndex);

	meanGraph.at(iParticle).at(iSubSpecies).at(yIndex) = new TGraphErrors();
	widthGraph.at(iParticle).at(iSubSpecies).at(yIndex) = new TGraphErrors();
	meanGraphTOF.at(iParticle).at(iSubSpecies).at(yIndex) = new TGraphErrors();
	widthGraphTOF.at(iParticle).at(iSubSpecies).at(yIndex) = new TGraphErrors();

	currentMeanGraph = meanGraph.at(iParticle).at(iSubSpecies).at(yIndex);
	currentWidthGraph = widthGraph.at(iParticle).at(iSubSpecies).at(yIndex);
	currentMeanGraphTOF = meanGraphTOF.at(iParticle).at(iSubSpecies).at(yIndex);
	currentWidthGraphTOF = widthGraphTOF.at(iParticle).at(iSubSpecies).at(yIndex);
	
	currentMeanGraph->SetName(Form("meanGraph_%s_%s_%d",
				       particleInfo->GetParticleName(particleSpecies).Data(),
				       particleInfo->GetParticleName(subSpecies).Data(),
				       yIndex));
	currentWidthGraph->SetName(Form("widthGraph_%s_%s_%d",
					particleInfo->GetParticleName(particleSpecies).Data(),
					particleInfo->GetParticleName(subSpecies).Data(),
					yIndex));
	
	currentMeanGraph->SetTitle(Form("zTPC Mean : Mass Assumption=%s : Species=%s : y=%.03g;m_{T}-m_{%s};ZTPC_{%s}^{%s}",
					particleInfo->GetParticleName(particleSpecies).Data(),
					particleInfo->GetParticleName(subSpecies).Data(),
					rapidity,
					particleInfo->GetParticleSymbol(particleSpecies).Data(),
					particleInfo->GetParticleSymbol(particleSpecies).Data(),
					particleInfo->GetParticleSymbol(subSpecies).Data()));
	currentWidthGraph->SetTitle(Form("zTPC Width : Mass Assumption=%s : Species=%s : y=%.03g;m_{T}-m_{%s};#sigma_{%s}^{%s}",
					 particleInfo->GetParticleName(particleSpecies).Data(),
					 particleInfo->GetParticleName(subSpecies).Data(),
					 rapidity,
					 particleInfo->GetParticleSymbol(particleSpecies).Data(),
					 particleInfo->GetParticleSymbol(particleSpecies).Data(),
					 particleInfo->GetParticleSymbol(subSpecies).Data()));
	
	currentMeanGraphTOF->SetName(Form("meanGraphTOF_%s_%s_%d",
					  particleInfo->GetParticleName(particleSpecies).Data(),
					  particleInfo->GetParticleName(subSpecies).Data(),
					  yIndex));
	currentWidthGraphTOF->SetName(Form("widthGraphTOF_%s_%s_%d",
					   particleInfo->GetParticleName(particleSpecies).Data(),
					   particleInfo->GetParticleName(subSpecies).Data(),
					   yIndex));
	
	currentMeanGraphTOF->SetTitle(Form("zTOF Mean : Mass Assumption=%s : Species=%s : y=%.03g;m_{T}-m_{%s};ZTOF_{%s}^{%s}",
					   particleInfo->GetParticleName(particleSpecies).Data(),
					   particleInfo->GetParticleName(subSpecies).Data(),
					   rapidity,
					   particleInfo->GetParticleSymbol(particleSpecies).Data(),
					   particleInfo->GetParticleSymbol(particleSpecies).Data(),
					   particleInfo->GetParticleSymbol(subSpecies).Data()));
	currentWidthGraphTOF->SetTitle(Form("zTOF Width : Mass Assumption=%s : Species=%s : y=%.03g;m_{T}-m_{%s};#sigma_{%s}^{%s}",
					    particleInfo->GetParticleName(particleSpecies).Data(),
					    particleInfo->GetParticleName(subSpecies).Data(),
					    rapidity,
					    particleInfo->GetParticleSymbol(particleSpecies).Data(),
					    particleInfo->GetParticleSymbol(particleSpecies).Data(),
					    particleInfo->GetParticleSymbol(subSpecies).Data()));
	
	currentMeanGraph->SetMarkerStyle(kFullCircle);
	currentWidthGraph->SetMarkerStyle(kFullCircle);
	currentMeanGraphTOF->SetMarkerStyle(kFullCircle);
	currentWidthGraphTOF->SetMarkerStyle(kFullCircle);
	
	currentMeanGraph->SetMarkerColor(particleInfo->GetParticleColor(subSpecies));
	currentWidthGraph->SetMarkerColor(particleInfo->GetParticleColor(subSpecies));
	currentMeanGraphTOF->SetMarkerColor(particleInfo->GetParticleColor(subSpecies));
	currentWidthGraphTOF->SetMarkerColor(particleInfo->GetParticleColor(subSpecies));
	
      }//End Loop Over yIndex to create graphs
    }//End Loop Over subspecies To Create Graphs
    //End Creating Mean and Width Graphs


    
    for (unsigned int iSubSpecies=0; iSubSpecies<nParticles; iSubSpecies++){
      int subSpecies = particles.at(iSubSpecies);

      for (unsigned int yIndex=0; yIndex<nRapidityBins; yIndex++){

	if (draw){
	  canvas->cd(4);
	  gPad->DrawFrame(0.0,-.8,1.3,.8);
	  gPad->Update();
	  canvas->cd(5);
	  gPad->DrawFrame(0.0,.006,1.3,.013);
	  gPad->Update();
	}
	
	LoadYieldHistograms(tofOptZTPCHisto3D.at(iParticle).at(iSubSpecies),
			    &tofOptZTPCHistos,"TPC",yIndex);
	LoadYieldHistograms(zTOFHisto3D.at(iParticle),&zTOFHistos,"TOF",yIndex);

	Double_t rapidity = GetRapidityRangeCenter(yIndex);
	if (!particleInfo->SetEmpiricalBichselFunction(particleSpecies,yIndex))
	  continue;

	currentMeanGraph = meanGraph.at(iParticle).at(iSubSpecies).at(yIndex);
	currentWidthGraph = widthGraph.at(iParticle).at(iSubSpecies).at(yIndex);
	
	for (unsigned int mTm0Index=0; mTm0Index<nmTm0Bins; mTm0Index++){

  
	  Double_t mTm0 = GetmTm0RangeCenter(mTm0Index);
	  if (mTm0 < .1)
	    continue;

	  Double_t mass = particleInfo->GetParticleMass(particleSpecies);
	  Double_t pT   = ConvertmTm0ToPt(mTm0,mass);
	  Double_t rapidity = GetRapidityRangeCenter(yIndex);
	  Double_t pZ   = ComputepZ(mTm0+mass,rapidity);
	  Double_t pTotal = ComputepTotal(pT,pZ);

	  currentHisto = tofOptZTPCHistos.at(yIndex).at(mTm0Index);
	  currentHistoTOF = zTOFHistos.at(yIndex).at(mTm0Index);

	  //cout <<"Particle: " <<particleSpecies <<" SubSpecies: " <<subSpecies
	  //     <<" yIndex: " <<yIndex <<" mTm0Index: " <<mTm0Index <<endl; 
	  
	  //------ zTPC Means and Widths ------
	  if (mTm0 <= 0.75 && currentHisto->GetEntries() > 100){
	    
	    if (draw){
	      canvas->cd(3);
	      currentHisto->Draw();
	      gPad->Update();
	    }
	    
	    TF1 fitFunc("","gaus",currentHisto->GetMean()-.2,
			currentHisto->GetMean()+.2);

	    fitFunc.SetParameter(1,particleInfo->PredictZTPC(pTotal,particleSpecies,subSpecies));
	    fitFunc.SetParameter(2,.07);
	    
	    if (draw)
	      currentHisto->Fit(&fitFunc,"RQ");
	    else
	      currentHisto->Fit(&fitFunc,"RQN");
	    
	    currentMeanGraph->SetPoint(currentMeanGraph->GetN(),mTm0,fitFunc.GetParameter(1));
	    currentMeanGraph->SetPointError(currentMeanGraph->GetN()-1,0,fitFunc.GetParError(1));
	    
	    currentWidthGraph->SetPoint(currentWidthGraph->GetN(),mTm0,fitFunc.GetParameter(2));
	    currentWidthGraph->SetPointError(currentWidthGraph->GetN()-1,0,fitFunc.GetParError(2));
	    
	    if (draw){
	      canvas->cd(1);
	      currentMeanGraph->Draw("AP");
	      canvas->cd(2);
	      currentWidthGraph->Draw("AP");
	      
	      canvas->Update();
	      gSystem->Sleep(500);
	    }
	    
	    delete currentHisto;
	  }//End zTPC Means and Widths

	  //----- zTOF Means and Widths
	  if (!currentHistoTOF){
	    cout <<"No ToF Histogram" <<endl;
	    continue;
	  }
	  
	  if (currentHistoTOF->GetEntries() > 100 && iSubSpecies%nParticles == 0 && mTm0 <= 1.2){

	    Double_t pionMeanPrediction = particleInfo->PredictZTOF(pTotal,particleSpecies,PION);
	    Double_t kaonMeanPrediction = particleInfo->PredictZTOF(pTotal,particleSpecies,KAON);
	    Double_t protonMeanPrediction = particleInfo->PredictZTOF(pTotal,particleSpecies,PROTON);

	    Double_t minRange = TMath::Min(TMath::Min(pionMeanPrediction,kaonMeanPrediction),
					   protonMeanPrediction)-.1;
	    Double_t maxRange = TMath::Max(TMath::Max(pionMeanPrediction,kaonMeanPrediction),
					   protonMeanPrediction)+.1;
	    
	    currentHistoTOF->GetXaxis()->SetRangeUser(minRange,maxRange);
	    
	    if (draw){
		canvas->cd(6);
		currentHistoTOF->Draw();
		gPad->Update();
	      }

	    
	    
	    TF1 fitFunc("","gaus(0)+gaus(3)+gaus(6)",minRange,maxRange);
	    fitFunc.SetNpx(100000);
	    fitFunc.SetParameter(0,currentHistoTOF->GetBinContent(currentHistoTOF->FindBin(pionMeanPrediction)));
	    fitFunc.SetParameter(1,pionMeanPrediction);
	    fitFunc.SetParameter(2,.012);
	    fitFunc.SetParameter(3,currentHistoTOF->GetBinContent(currentHistoTOF->FindBin(kaonMeanPrediction)));
	    fitFunc.SetParameter(4,kaonMeanPrediction);
	    fitFunc.SetParameter(5,.012);
	    fitFunc.SetParameter(6,currentHistoTOF->GetBinContent(currentHistoTOF->FindBin(protonMeanPrediction)));
	    fitFunc.SetParameter(7,protonMeanPrediction);
	    fitFunc.SetParameter(8,.012);

	    fitFunc.SetParLimits(0,0,fitFunc.GetParameter(0)*1.1);
	    fitFunc.SetParLimits(1,fitFunc.GetParameter(1)-fitFunc.GetParameter(2)*.5,
				 fitFunc.GetParameter(1)+fitFunc.GetParameter(2)*.5);
	    fitFunc.SetParLimits(2,.001,.014);
	    fitFunc.SetParLimits(3,0,fitFunc.GetParameter(3)*1.1);
	    fitFunc.SetParLimits(4,fitFunc.GetParameter(4)-fitFunc.GetParameter(5)*.5,
				 fitFunc.GetParameter(4)+fitFunc.GetParameter(5)*.5);
	    fitFunc.SetParLimits(5,.001,.014);
	    fitFunc.SetParLimits(6,0,fitFunc.GetParameter(6)*1.1);
	    fitFunc.SetParLimits(7,fitFunc.GetParameter(7)-fitFunc.GetParameter(8)*.5,
				 fitFunc.GetParameter(7)+fitFunc.GetParameter(8)*.5);
	    fitFunc.SetParLimits(8,.001,.014);
	    

	    if (draw){
	      currentHistoTOF->Fit(&fitFunc,"RQ");
	    }
	    else
	      currentHistoTOF->Fit(&fitFunc,"RQN");

	    for (unsigned int i=0; i<nParticles; i++){
	      meanGraphTOF.at(iParticle).at(i).at(yIndex)->
		SetPoint(meanGraphTOF.at(iParticle).at(i).at(yIndex)->GetN(),mTm0,
			 fitFunc.GetParameter((i*nParticles)+1));
	      meanGraphTOF.at(iParticle).at(i).at(yIndex)->
		SetPointError(meanGraphTOF.at(iParticle).at(i).at(yIndex)->GetN()-1,0,
			      fitFunc.GetParError((i*nParticles)+1));
	      
	      widthGraphTOF.at(iParticle).at(i).at(yIndex)->
		SetPoint(widthGraphTOF.at(iParticle).at(i).at(yIndex)->GetN(),mTm0,
			 fitFunc.GetParameter((i*nParticles)+2));
	      widthGraphTOF.at(iParticle).at(i).at(yIndex)->
		SetPointError(widthGraphTOF.at(iParticle).at(i).at(yIndex)->GetN()-1,0,
			      fitFunc.GetParError((i*nParticles)+2));
	    }
	    
	    if (draw){
	      canvas->cd(4);
	      meanGraphTOF.at(iParticle).at(0).at(yIndex)->Draw("P");
	      meanGraphTOF.at(iParticle).at(1).at(yIndex)->Draw("P");
	      meanGraphTOF.at(iParticle).at(2).at(yIndex)->Draw("P");
	      gPad->Update();
	      canvas->cd(5);
	      widthGraphTOF.at(iParticle).at(0).at(yIndex)->Draw("P");
	      widthGraphTOF.at(iParticle).at(1).at(yIndex)->Draw("P");
	      widthGraphTOF.at(iParticle).at(2).at(yIndex)->Draw("P");

	      gPad->Update();
	    }
	  }//End TOF Means and Widths
	  
	}//End Loop Over mTm0Index


	//----TPC Width Parameters -----
	
	//Make sure the mean and width graph have points
	if (currentWidthGraph->GetN() < 1 || currentMeanGraph->GetN() < 1){
	  delete currentMeanGraph;
	  delete currentWidthGraph;
	  continue;
	}
	//cout <<"Parameterizing TPC Width" <<endl;
	//Fit Range of the Width Graph
	Double_t minRange = currentWidthGraph->GetX()[0];
	Double_t maxRange =  currentWidthGraph->GetX()[currentWidthGraph->GetN()-1];

	//Fit the Width graph with three different polys and determine which has the best chi2
	std::vector<double> chi2 (3);
	TString fitForm[3] = {"pol1","pol3","pol5"};
	TF1 pol1Fit("",fitForm[0],minRange,maxRange);
	TF1 pol3Fit("",fitForm[1],minRange,maxRange);
	TF1 pol5Fit("",fitForm[2],minRange,maxRange);

	currentWidthGraph->Fit(&pol1Fit,"RNQ");
	chi2.at(0) = fabs((pol1Fit.GetChisquare() / (double) pol1Fit.GetNDF()) - 1);
	currentWidthGraph->Fit(&pol3Fit,"RNQ");
	chi2.at(1) =fabs((pol3Fit.GetChisquare() / (double) pol3Fit.GetNDF()) - 1);
	currentWidthGraph->Fit(&pol5Fit,"RNQ");
	chi2.at(2) = fabs((pol5Fit.GetChisquare() / (double) pol5Fit.GetNDF()) - 1);
	
	int bestFitIndex = TMath::LocMin(3,&chi2.at(0));
	TF1 widthFitFunc("",fitForm[bestFitIndex],minRange,maxRange);
	widthFitFunc.SetName(Form("widthFit_%s_%s_%d",
				  particleInfo->GetParticleName(particleSpecies).Data(),
				  particleInfo->GetParticleName(subSpecies).Data(),
				  yIndex));
	widthFitFunc.SetNpx(10000);
	widthFitFunc.SetLineColor(particleInfo->GetParticleColor(subSpecies));
	widthFitFunc.SetLineWidth(2);
	
	currentWidthGraph->Fit(&widthFitFunc,"RQ");

	//Get the Confidence Interval
	TGraphErrors confInterval(widthFitFunc.GetNpx());
	confInterval.SetName(Form("widthFitConf_%s_%s_%d",
				  particleInfo->GetParticleName(particleSpecies).Data(),
				  particleInfo->GetParticleName(subSpecies).Data(),
				  yIndex));
	Double_t step = (maxRange-minRange)/(double)confInterval.GetN();
	for (int i=0; i<confInterval.GetN(); i++){
	  confInterval.SetPoint(i,minRange + i*step,0);
	}
	(TVirtualFitter::GetFitter())->GetConfidenceIntervals(&confInterval,.68);
	confInterval.SetFillColor(particleInfo->GetParticleColor(subSpecies));
	confInterval.SetFillStyle(3001);

	if (draw){
	  canvas->cd(2);
	  confInterval.Draw("2");
	  widthFitFunc.Draw("SAME");
	  gPad->Update();
	  gSystem->Sleep(50);
	}
	
	//---- TOF Width Parameter Fits -----
	//cout <<"Parameterizing TOF Width" <<endl;
	std::vector<TF1 *> widthFitFuncsTOF(nParticles,(TF1*)NULL);
	std::vector<TGraphErrors *> widthFitTOFConfInterval(nParticles,(TGraphErrors *)NULL);
	if (iSubSpecies%nParticles == 0){
	  
	  for (unsigned int i=0; i<nParticles; i++){
	    TGraphErrors *currentTOFWidthGraph = widthGraphTOF.at(iParticle).at(i).at(yIndex);
	    
	    if (!currentTOFWidthGraph){
	      cout <<"No Tof width graph" <<endl;
	      continue;
	    }
	    
	    if (currentTOFWidthGraph->GetN() < 3)
	      continue;
	    
	    //Fit Range of The TOF Width Graph
	    Double_t minTOFWidthRange = currentTOFWidthGraph->GetX()[0];
	    Double_t maxTOFWidthRange = currentTOFWidthGraph->GetX()[currentTOFWidthGraph->GetN()-1];
	    
	    std::vector<double> chi2TOF (3);
	    TString fitFormTOF[3] = {"pol1","pol3","pol5"};
	    TF1 pol1FitTOF("",fitFormTOF[0],minTOFWidthRange,maxTOFWidthRange);
	    TF1 pol3FitTOF("",fitFormTOF[1],minTOFWidthRange,maxTOFWidthRange);
	    TF1 pol5FitTOF("",fitFormTOF[2],minTOFWidthRange,maxTOFWidthRange);
	    
	    currentTOFWidthGraph->Fit(&pol1FitTOF,"RNQ");
	    chi2TOF.at(0) = fabs((pol1FitTOF.GetChisquare() / (double) pol1FitTOF.GetNDF()) - 1);
	    currentTOFWidthGraph->Fit(&pol3FitTOF,"RNQ");
	    chi2TOF.at(1) =fabs((pol3FitTOF.GetChisquare() / (double) pol3FitTOF.GetNDF()) - 1);
	    currentTOFWidthGraph->Fit(&pol5FitTOF,"RNQ");
	    chi2TOF.at(2) = fabs((pol5FitTOF.GetChisquare() / (double) pol5FitTOF.GetNDF()) - 1);
	    
	    Int_t bestFitIndexTOF = TMath::LocMin(3,&chi2TOF.at(0));
	    widthFitFuncsTOF.at(i) = new TF1("",fitFormTOF[bestFitIndexTOF],minTOFWidthRange,maxTOFWidthRange);
	    widthFitFuncsTOF.at(i)->SetName(Form("widthFitTOF_%s_%s_%d",
						 particleInfo->GetParticleName(particleSpecies).Data(),
						 particleInfo->GetParticleName(i).Data(),
						 yIndex));
	    widthFitFuncsTOF.at(i)->SetNpx(10000);
	    widthFitFuncsTOF.at(i)->SetLineWidth(2);
	    widthFitFuncsTOF.at(i)->SetLineColor(particleInfo->GetParticleColor(i));
	    
	    currentTOFWidthGraph->Fit(widthFitFuncsTOF.at(i),"RQ");
	    
	    //Get the Confidence Interval
	    
	    widthFitTOFConfInterval.at(i) = new TGraphErrors(widthFitFuncsTOF.at(i)->GetNpx());
	    widthFitTOFConfInterval.at(i)->SetName(Form("widthFitConfTOF_%s_%s_%d",
							particleInfo->GetParticleName(particleSpecies).Data(),
							particleInfo->GetParticleName(i).Data(),
							yIndex));
	    Double_t stepTOF = (maxTOFWidthRange-minTOFWidthRange)/(double)widthFitTOFConfInterval.at(i)->GetN();
	    for (int j=0; j<widthFitTOFConfInterval.at(i)->GetN(); j++){
	      widthFitTOFConfInterval.at(i)->SetPoint(j,minTOFWidthRange+j*stepTOF,0);
	    }
	    (TVirtualFitter::GetFitter())->GetConfidenceIntervals(widthFitTOFConfInterval.at(i),.68);
	    widthFitTOFConfInterval.at(i)->SetFillColor(particleInfo->GetParticleColor(i));
	    widthFitTOFConfInterval.at(i)->SetFillStyle(3001);
	    
	    
	    if (draw){
	      canvas->cd(5);
	      widthFitTOFConfInterval.at(i)->Draw("2");
	      widthFitFuncsTOF.at(i)->Draw("SAME");
	      gPad->Update();
	      canvas->Update();
	      gSystem->Sleep(50);
	    }
	    
	    
	  }//End Loop Over TOF SubSpecies
	}
	
	//Save	
	pidFile->cd();
	pidFile->cd("MeanAndWidthGraphs");
	currentMeanGraph->Write();
	currentWidthGraph->Write();
	if (iSubSpecies%nParticles == 0){
	  for (unsigned int i=0; i<nParticles; i++){
	    if (meanGraphTOF.at(iParticle).at(i).at(yIndex)->GetN() > 3){
	      meanGraphTOF.at(iParticle).at(i).at(yIndex)->Write();
	      widthGraphTOF.at(iParticle).at(i).at(yIndex)->Write();
	    }
	  }
	}
	
	pidFile->cd();
	pidFile->cd("MeanAndWidthFits");
	widthFitFunc.Write();
	confInterval.Write();
	if (iSubSpecies%nParticles == 0){
	  for (unsigned int i=0; i<nParticles; i++){
	    if (widthFitFuncsTOF.at(i)){
	      widthFitFuncsTOF.at(i)->Write();
	      widthFitTOFConfInterval.at(i)->Write();
	    }
	  }
	}
	
	
	//Clean Up
	if (currentMeanGraph)
	  delete currentMeanGraph;
	if (currentWidthGraph)
	  delete currentWidthGraph;
	for (unsigned int i=0; i<nParticles; i++){
	  if (meanGraphTOF.at(iParticle).at(i).at(yIndex)){
	    delete meanGraphTOF.at(iParticle).at(i).at(yIndex);
	    meanGraphTOF.at(iParticle).at(i).at(yIndex) = NULL;
	  }
	  if (widthGraphTOF.at(iParticle).at(i).at(yIndex)){
	    delete widthGraphTOF.at(iParticle).at(i).at(yIndex);
	    widthGraphTOF.at(iParticle).at(i).at(yIndex) = NULL;
	  }
	  if (widthFitFuncsTOF.at(i)){
	    delete widthFitFuncsTOF.at(i);
	    widthFitFuncsTOF.at(i) = NULL;
	  }
	}
	
      }//End Loop Over yIndex
      

    }//End Loop Over Sub Species
    
  }//End Loop Over Particles
  
}
			   
