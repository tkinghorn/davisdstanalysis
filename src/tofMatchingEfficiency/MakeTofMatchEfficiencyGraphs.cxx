#include <iostream>
#include <utility>
#include <vector>

#include <TString.h>
#include <TFile.h>
#include <TTree.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TH3D.h>
#include <TMath.h>
#include <TF1.h>
#include <TGraphAsymmErrors.h>

#include "globalDefinitions.h"
#include "utilityFunctions.h"
#include "TrackInfo.h"
#include "PrimaryVertexInfo.h"
#include "EventInfo.h"
#include "DavisDstReader.h"
#include "ParticleInfo.h"
#include "UserCuts.h"

void MakeTofMatchEfficiencyGraphs(TString file){

  TFile *tofFile = new TFile(file,"UPDATE");

  tofFile->mkdir("TofEfficiency");
  tofFile->mkdir("TofMisMatch");

  TH2F *tpcTracks = NULL;
  TH2F *tofTracks = NULL;
  TH2F *tofMisMatch = NULL;

  ParticleInfo *particleInfo = new ParticleInfo();

  const int nCentralityBins = GetNCentralityBins();
  const int nParticles = particleInfo->GetNumberOfDefinedParticles();

  std::vector<std::vector<std::vector<TGraphAsymmErrors *> > >
    tofEffGraph(nCentralityBins,std::vector<std::vector<TGraphAsymmErrors *> >
		(nParticles, std::vector<TGraphAsymmErrors *>
		 (nRapidityBins,new TGraphAsymmErrors())));

  std::vector<std::vector<std::vector<TGraphAsymmErrors *> > >
    tofMisMatchGraph(nCentralityBins,std::vector<std::vector<TGraphAsymmErrors *> >
		     (nParticles, std::vector<TGraphAsymmErrors *>
		      (nRapidityBins,new TGraphAsymmErrors())));
  
  //Create the TofEfficiency Graphs
  for (int iCentBin=0; iCentBin<nCentralityBins; iCentBin++){
    for (int iParticle=0; iParticle<nParticles; iParticle++){

      //Get the Tracks Histos
      tofFile->cd();
      tofFile->cd("TrackHistos");
      tpcTracks = (TH2F *)gDirectory->
	Get(Form("tpcTracks_Cent%d_%s",
		 iCentBin,particleInfo->GetParticleName(iParticle).Data()));
      tofTracks = (TH2F *)gDirectory->
	Get(Form("tofTracks_Cent%d_%s",
		 iCentBin,particleInfo->GetParticleName(iParticle).Data()));
      tofMisMatch = (TH2F *)gDirectory->
	Get(Form("tofMismatches_Cent%d_%s",
		 iCentBin,particleInfo->GetParticleName(iParticle).Data()));
      
      //Make Sure the Graphs Exist
      if (!tpcTracks || !tofTracks || !tofMisMatch)
	continue;

      //Loop Over the Rapidity Bins
      for (int yIndex=0; yIndex<tpcTracks->GetNbinsX()-1; yIndex++){
	
	TH1D *tpcTemp = tpcTracks->ProjectionY(Form("tpcTemp_%d_%d_%d",
							   iCentBin,iParticle,yIndex),
						      yIndex+1,yIndex+1);
	TH1D *tofTemp = tofTracks->ProjectionY(Form("tofTemp_%d_%d_%d",
							   iCentBin,iParticle,yIndex),
						      yIndex+1,yIndex+1);
	TH1D *misMatchTemp = tofMisMatch->ProjectionY(Form("misMatchTemp_%d_%d_%d",
							   iCentBin,iParticle,yIndex),
						      yIndex+1,yIndex+1);
	
	tofEffGraph.at(iCentBin).at(iParticle).at(yIndex)->BayesDivide(tofTemp,tpcTemp);
	tofMisMatchGraph.at(iCentBin).at(iParticle).at(yIndex)->BayesDivide(misMatchTemp,tofTemp);
	
	tofEffGraph.at(iCentBin).at(iParticle).at(yIndex)->
	  SetName(Form("tofMatchingEfficiency_Cent%d_%s_%d",
		       iCentBin,
		       particleInfo->GetParticleName(iParticle).Data(),
		       yIndex));
	tofEffGraph.at(iCentBin).at(iParticle).at(yIndex)->
	  SetTitle(Form("TOF Matching Efficiency | %s | Cent:%d | y=[%.03g,%.03g];m_{T}-m_{%s};nTOF/nTPC",
			particleInfo->GetParticleName(iParticle).Data(),
			iCentBin,GetRapidityRangeLow(yIndex),GetRapidityRangeHigh(yIndex),
			particleInfo->GetParticleSymbol(iParticle).Data()));

	tofMisMatchGraph.at(iCentBin).at(iParticle).at(yIndex)->
	  SetName(Form("tofMisMatchRate_Cent%d_%s_%d",
		       iCentBin,
		       particleInfo->GetParticleName(iParticle).Data(),
		       yIndex));
	tofMisMatchGraph.at(iCentBin).at(iParticle).at(yIndex)->
	  SetTitle(Form("TOF Mis-Match Rate | %s | Cent:%d | y=[%.03g,%.03g];m_{T}-m_{%s};Mismatch Rate",
			particleInfo->GetParticleName(iParticle).Data(),
			iCentBin,GetRapidityRangeLow(yIndex),GetRapidityRangeHigh(yIndex),
			particleInfo->GetParticleSymbol(iParticle).Data()));
	
	tofFile->cd();
	tofFile->cd("TofEfficiency");
	tofEffGraph.at(iCentBin).at(iParticle).at(yIndex)->Write();

	tofFile->cd();
	tofFile->cd("TofMisMatch");
	tofMisMatchGraph.at(iCentBin).at(iParticle).at(yIndex)->Write();
	
      }//End Loop Over rapidity bins

	
      delete tpcTracks;
      delete tofTracks;
      delete tofMisMatch;
      
      tpcTracks = NULL;
      tofTracks = NULL;
      tofMisMatch = NULL;
      
    }//End Loop Over particle species
  }//End Loop Over Centrality Bins

}
