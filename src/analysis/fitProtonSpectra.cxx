//Fit the Proton Spectra

#include <iostream>
#include <vector>
#include <utility>

#include <TStyle.h>
#include <TMath.h>
#include <TFile.h>
#include <TF1.h>
#include <TCanvas.h>
#include <TMultiGraph.h>
#include <TGraphErrors.h>
#include <TGraphAsymmErrors.h>
#include <TSystem.h>
#include <TVirtualFitter.h>
#include <TAxis.h>
#include <TDirectory.h>
#include <TH1F.h>
#include <TLine.h>

#include "globalDefinitions.h"
#include "utilityFunctions.h"
#include "StRefMultExtendedCorr.h"
#include "UserCuts.h"
#include "ParticleInfo.h"
#include "SpectraClass.h"
#include "SpectraFitUtilities.h"
#include "SpectraFitFunctions.h"
#include "SimFitter.h"
#include "StyleSettings.h"

bool draw = false;

//_________________________________________________________________________
void fitProtonSpectra(TString spectraFileName, Int_t charge,
		      Int_t userCentBin = -999, Double_t userRapidity = -999){

  gStyle->SetOptFit(1);
  ParticleInfo *particleInfo = new ParticleInfo();
  const unsigned int pid = PROTON;
  const double pidMass = particleInfo->GetParticleMass(pid);
  const unsigned int nCentBins = GetNCentralityBins();

  //Open the SpectraFile
  TFile *spectraFile = new TFile(spectraFileName,"UPDATE");
  TString spectraClassDir = TString::Format("SpectraClass_%s",
					    particleInfo->GetParticleName(pid,charge).Data());
  TString spectrumDir = TString::Format("CorrectedSpectra_%s",
					particleInfo->GetParticleName(pid,charge).Data());
  TString spectrumFitDir = TString::Format("%s_Fit",
					   spectrumDir.Data());
  TString dNdyDir = TString::Format("dNdyGraph_%s",
				    particleInfo->GetParticleName(pid,charge).Data());


  
  //Create a canvas if drawing
  TCanvas *canvas = NULL;
  TH1F *spectraFrame = NULL;
  TCanvas *parCanvas = NULL;
  if (draw){
    canvas = new TCanvas("canvas","canvas",20,20,800,600);
    gPad->SetLogy();
    spectraFrame = canvas->DrawFrame(0,.1,2,100);
    parCanvas = new TCanvas("parCanvas","parCanvas",20,20,1600,600);
    parCanvas->Divide(3,1);
  }
  
  //Create the Fit Functions
  TF1 *siemensFunc = new TF1("siemensFunc",SiemensRasmussen,0.001,2,4);
  siemensFunc->SetNpx(1000);
  siemensFunc->SetParName(0,"A");
  siemensFunc->SetParName(1,"T");
  siemensFunc->SetParName(2,"#beta_{r}");
  siemensFunc->SetParName(3,Form("m_{%s}",particleInfo->GetParticleSymbol(pid).Data()));

  TF1 *heinzBlastWaveFunc = new TF1("heinzBlastWaveFunc",BlastWaveModelFit,0.001,2,6);
  heinzBlastWaveFunc->SetNpx(100);
  heinzBlastWaveFunc->SetParName(0,"A");
  heinzBlastWaveFunc->SetParName(1,"T_{Kin}");
  heinzBlastWaveFunc->SetParName(2,"#beta_{s}");
  heinzBlastWaveFunc->SetParName(3,"n");
  heinzBlastWaveFunc->SetParName(4,"R");
  heinzBlastWaveFunc->SetParName(5,Form("m_{%s}",particleInfo->GetParticleSymbol(pid).Data()));
  

  //Load the Spectra
  //The most internal vector holds all the spectra of equal |y|

  std::vector<std::vector<std::vector<SpectraClass *> > > spectra
    (nCentBins,std::vector<std::vector<SpectraClass *> >
     (nRapidityBins, std::vector<SpectraClass *>
      (0,(SpectraClass *)NULL)));
  std::vector<std::vector<std::vector<TGraphErrors *> > > spectraToFit
    (nCentBins,std::vector<std::vector<TGraphErrors *> >
     (nRapidityBins, std::vector<TGraphErrors *>
      (0,(TGraphErrors *)NULL)));
  std::vector<std::vector<std::vector<TF1 *> > > fitFuncs //Seimens-Rassmussen Blast Wave
    (nCentBins,std::vector<std::vector<TF1 *> >
     (nRapidityBins, std::vector<TF1 *>
      (0,(TF1 *)NULL)));
  std::vector<std::vector<std::vector<TF1 *> > > blastWaveFitFuncs //Heinz Blast Wave
    (nCentBins,std::vector<std::vector<TF1 *> >
     (nRapidityBins, std::vector<TF1 *>
      (0,(TF1 *)NULL))); 
  for (unsigned int iCentBin=0; iCentBin<nCentBins; iCentBin++){
    for (unsigned int yIndex=GetMinRapidityIndexOfInterest(pid); yIndex<=nRapidityBins/2; yIndex++){
      
      std::vector<int> yIndexVec;
      yIndexVec.push_back(yIndex);
      if ((int)yIndex != GetRapidityIndex(-1.*GetRapidityRangeCenter(yIndex)))
	yIndexVec.push_back(GetRapidityIndex(-1.*GetRapidityRangeCenter(yIndex)));

      for (unsigned int iSpectra=0; iSpectra<yIndexVec.size(); iSpectra++){
	TString spectraName = TString::Format("CorrectedSpectra_%s_Cent%02d_yIndex%02d_Total",
					      particleInfo->GetParticleName(pid,charge).Data(),
					      iCentBin,yIndexVec.at(iSpectra));
	cout <<spectraName.Data() <<endl;

	spectraFile->cd(spectraClassDir.Data());
	SpectraClass *temp = (SpectraClass *)gDirectory->Get(spectraName.Data());

	if (temp){
	  spectra.at(iCentBin).at(yIndex).push_back(temp);
	  spectra.at(iCentBin).at(yIndex).back()->SetSpectraFile(spectraFile);
	  spectra.at(iCentBin).at(yIndex).back()->SetSpectraClassDir(spectraClassDir);
	  spectra.at(iCentBin).at(yIndex).back()->SetSpectraDir(spectrumDir);
	  spectra.at(iCentBin).at(yIndex).back()->SetSpectraNamePrefix("correctedSpectra");
	  spectra.at(iCentBin).at(yIndex).back()->LoadSpectra();
	  spectraToFit.at(iCentBin).at(yIndex).push_back(spectra.at(iCentBin).at(yIndex).back()->GetTGraph());

	  fitFuncs.at(iCentBin).at(yIndex).push_back(new TF1(*siemensFunc));
	  fitFuncs.at(iCentBin).at(yIndex).back()->SetName(Form("%s_Fit0",spectraName.Data()));

	  blastWaveFitFuncs.at(iCentBin).at(yIndex).push_back(new TF1(*heinzBlastWaveFunc));
	  blastWaveFitFuncs.at(iCentBin).at(yIndex).back()->SetName(Form("%s_Fit1",spectraName.Data()));
	  
	}
	
      }//End Loop Over the spectra for this |y|      
    }//End Loop Over Rapidity Bins
  }//End Loop Over Centrality Bins

 
  //Create the Graphs to hold the fit parameters
  std::vector<std::vector<TGraphErrors *> > fitParGraphs
    (nCentBins, std::vector<TGraphErrors *>
     (siemensFunc->GetNpar()-1,(TGraphErrors *)NULL));
  

  if (draw){
    for (uint iPar=0; iPar<fitParGraphs.at(0).size(); iPar++){
      parCanvas->cd(iPar+1);
      gPad->DrawFrame(-2,0,2,iPar==0 ? 100 : 1);
    }
  }
  
  //Loop Over the Centrality Bins
  //bool useThermalFit = true;
  for (unsigned int iCentBin=0; iCentBin < nCentBins; iCentBin++){
    
    //Skip Bins that are not requested by the user
    if (userCentBin != -999 && userCentBin >= 0 && (int)iCentBin != userCentBin)
      continue;

    //Initialize the FitParGraph
    for (uint iPar=0; iPar<fitParGraphs.at(iCentBin).size(); iPar++){
      fitParGraphs.at(iCentBin).at(iPar) = new TGraphErrors();
      fitParGraphs.at(iCentBin).at(iPar)->SetName(Form("SpectraFitPar_%s_Cent%02d_par%02d",
						       particleInfo->GetParticleName(pid,charge).Data(),
						       iCentBin,iPar));
      fitParGraphs.at(iCentBin).at(iPar)->SetMarkerStyle(kFullCircle);
      fitParGraphs.at(iCentBin).at(iPar)->SetMarkerColor(GetCentralityColor(iCentBin));
    }

    //***************************************************************
    //ROUND 1 RADIAL VELOCITY
    for (unsigned int yIndex=GetMinRapidityIndexOfInterest(pid); yIndex<=nRapidityBins/2; yIndex++){
 
      if (spectra.at(iCentBin).at(yIndex).size() == 0)
	continue;

      //Define the Parameter Relationships
      enum fitParsIndex {TEMP,RADIALVELOCITY,PMASS,AMP};
      std::vector<std::vector<int> > parRelations (spectraToFit.at(iCentBin).at(yIndex).size(),std::vector<int> (0));
      for (unsigned int i=0; i<spectra.at(iCentBin).at(yIndex).size(); i++){
	parRelations.at(i).push_back(AMP+i);
	parRelations.at(i).push_back(TEMP);
	parRelations.at(i).push_back(RADIALVELOCITY);
	parRelations.at(i).push_back(PMASS);
      }
      
      //Seed the Total Fit Parameters
      std::vector<double> totalPars;
      totalPars.push_back(.13);
      totalPars.push_back(.69);
      totalPars.push_back(particleInfo->GetParticleMass(pid));
      for (unsigned int i=0; i<spectra.at(iCentBin).at(yIndex).size(); i++){
	totalPars.push_back(300000);
      }
      
      //Create the SimFitter
      SimFitter fitter(spectraToFit.at(iCentBin).at(yIndex),fitFuncs.at(iCentBin).at(yIndex));
      fitter.Init(totalPars);
      fitter.SetPrintLevel(0);
      fitter.SetParameterRelationships(parRelations);
      fitter.FixParameter(PMASS);
      fitter.SetParLimits(TEMP,.08,.3);
      fitter.SetParLimits(RADIALVELOCITY,0.001,.8);
      for (unsigned int i=0; i<spectra.at(iCentBin).at(yIndex).size(); i++){
	fitter.SetParLimits(AMP+i,2000,100000000);
      }

      //Do the Fit
      ROOT::Fit::FitResult results = fitter.Fit();
      std::vector<TF1 *> fitFuncResults = fitter.GetFitFunctions();
      
      
      if (fabs(results.Chi2()/results.Ndf()-1) < 5){
	fitParGraphs.at(iCentBin).at(2)->SetPoint(fitParGraphs.at(iCentBin).at(2)->GetN(),
						  GetRapidityRangeCenter(spectra.at(iCentBin).at(yIndex).at(0)->GetRapidityIndex()),
						  fitFuncResults.at(0)->GetParameter(2));
	fitParGraphs.at(iCentBin).at(2)->SetPointError(fitParGraphs.at(iCentBin).at(2)->GetN()-1,
						       rapidityBinWidth/2.0,
						       fitFuncResults.at(0)->GetParError(2));
      }

      //**** DRAW ****
      if (draw){
	canvas->cd();
	spectraFrame->GetYaxis()->SetRangeUser(fitFuncResults.at(0)->GetMinimum(),
					       fitFuncResults.at(0)->GetMaximum()*5);
	for (uint iFunc=0; iFunc<fitFuncResults.size(); iFunc++){
	  spectraToFit.at(iCentBin).at(yIndex).at(iFunc)->Draw("PZ");
	  fitFuncResults.at(iFunc)->Draw("SAME");
	}
	canvas->Update();

	parCanvas->cd();
	for (uint iPar=0; iPar<fitParGraphs.at(iCentBin).size(); iPar++){
	  parCanvas->cd(iPar+1);
	  fitParGraphs.at(iCentBin).at(iPar)->Draw("PZ");
	}
	parCanvas->Update();
	gSystem->Sleep(10);

      }

      
      //Clean UP
      for (uint i=0; i<fitFuncResults.size(); i++){
	delete fitFuncResults.at(i);
      }
      
    }//End Loop Over Rapidity Bins

    //Compute the Mean and RMS Radial Velocities
    std::vector<double> radialVelocityWeights;
    for (int i=0; i<fitParGraphs.at(iCentBin).at(2)->GetN(); i++){
      radialVelocityWeights.push_back(pow(1.0/fitParGraphs.at(iCentBin).at(2)->GetEY()[i],2));
    }
    double meanRadialVelocity(0), rmsRadialVelocity(0);    
    meanRadialVelocity = TMath::Mean(fitParGraphs.at(iCentBin).at(2)->GetN(),fitParGraphs.at(iCentBin).at(2)->GetY(),
				     &radialVelocityWeights.at(0));
    rmsRadialVelocity = TMath::RMS(fitParGraphs.at(iCentBin).at(2)->GetN(),fitParGraphs.at(iCentBin).at(2)->GetY(),
				   &radialVelocityWeights.at(0));

    if (draw){
      TLine *radialVelocityLine = new TLine(-2,meanRadialVelocity,2,meanRadialVelocity);
      radialVelocityLine->SetLineColor(fitParGraphs.at(iCentBin).at(2)->GetMarkerColor());
      radialVelocityLine->SetLineWidth(2);
      parCanvas->cd(3);
      radialVelocityLine->Draw("SAME");
    }
    
    //***************************************************************
    //ROUND 2 EXTRACT DNDY (fix Radial velocity)
    //Loop Over the Rapidity Bins
    for (unsigned int yIndex=GetMinRapidityIndexOfInterest(pid); yIndex<=nRapidityBins/2; yIndex++){

      if (spectra.at(iCentBin).at(yIndex).size() == 0)
	continue;
      
      //Define the Parameter Relationships
      enum fitParsIndex {TEMP,RADIALVELOCITY,PMASS,AMP};
      std::vector<std::vector<int> > parRelations (spectraToFit.at(iCentBin).at(yIndex).size(),std::vector<int> (0));
      for (unsigned int i=0; i<spectra.at(iCentBin).at(yIndex).size(); i++){
	parRelations.at(i).push_back(AMP+i);
	parRelations.at(i).push_back(TEMP);
	parRelations.at(i).push_back(RADIALVELOCITY);
	parRelations.at(i).push_back(PMASS);
      }

      
      //Seed the Total Fit Parameters
      std::vector<double> totalPars;
      totalPars.push_back(.13);
      totalPars.push_back(meanRadialVelocity);
      totalPars.push_back(particleInfo->GetParticleMass(pid));
      for (unsigned int i=0; i<spectra.at(iCentBin).at(yIndex).size(); i++){
	totalPars.push_back(300000);
      }
      
      //Create the SimFitter
      SimFitter fitter(spectraToFit.at(iCentBin).at(yIndex),fitFuncs.at(iCentBin).at(yIndex));
      fitter.Init(totalPars);
      fitter.SetPrintLevel(0);
      fitter.SetParameterRelationships(parRelations);
      fitter.FixParameter(RADIALVELOCITY);
      fitter.FixParameter(PMASS);
      fitter.SetParLimits(TEMP,.08,.3);
      //fitter.SetParLimits(RADIALVELOCITY,0.001,.8);
      for (unsigned int i=0; i<spectra.at(iCentBin).at(yIndex).size(); i++){
	fitter.SetParLimits(AMP+i,200,100000000);
      }

      ROOT::Fit::FitResult results = fitter.Fit();
      std::vector<TF1 *> fitFuncResults = fitter.GetFitFunctions();
      
      cout <<"Chi2/NDF: " <<results.Chi2() <<" / " <<results.Ndf() <<endl;


      //Add the Fit to the Spectrum
      for (unsigned int i=0; i<spectra.at(iCentBin).at(yIndex).size(); i++){
	fitFuncResults.at(i)->SetName(fitFuncs.at(iCentBin).at(yIndex).at(i)->GetName());
	spectra.at(iCentBin).at(yIndex).at(i)->AddSpectraFit(fitFuncResults.at(i),fitter.GetCovarianceMatrix(i),0);
	cout <<"dNdy: " <<spectra.at(iCentBin).at(yIndex).at(i)->GetdNdy(0) <<"+-"
	     <<spectra.at(iCentBin).at(yIndex).at(i)->GetdNdyErr(0) <<endl;
      }

      //Move the Fit Parameters to their graphs
      for (uint iPar=0; iPar<fitParGraphs.at(iCentBin).size(); iPar++){
	if (iPar==2) continue;
	for (uint iFunc=0; iFunc<spectra.at(iCentBin).at(yIndex).size(); iFunc++){
	  fitParGraphs.at(iCentBin).at(iPar)->SetPoint(fitParGraphs.at(iCentBin).at(iPar)->GetN(),
						       GetRapidityRangeCenter(spectra.at(iCentBin).at(yIndex).at(iFunc)->GetRapidityIndex()),
						       iPar == 0 ?
						       spectra.at(iCentBin).at(yIndex).at(iFunc)->GetdNdy(0) :
						       fitFuncResults.at(iFunc)->GetParameter(iPar));
	  fitParGraphs.at(iCentBin).at(iPar)->SetPointError(fitParGraphs.at(iCentBin).at(iPar)->GetN()-1,
							    rapidityBinWidth/2.0,
							    iPar == 0 ?
							    spectra.at(iCentBin).at(yIndex).at(iFunc)->GetdNdyErr(0) :
							    fitFuncResults.at(iFunc)->GetParError(iPar));
	}
      }
      

      //**** DRAW ****
      if (draw){
	canvas->cd();
	spectraFrame->GetYaxis()->SetRangeUser(fitFuncResults.at(0)->GetMinimum(),
					       fitFuncResults.at(0)->GetMaximum()*5);
	for (uint iFunc=0; iFunc<fitFuncResults.size(); iFunc++){
	  spectraToFit.at(iCentBin).at(yIndex).at(iFunc)->Draw("PZ");
	  fitFuncResults.at(iFunc)->Draw("SAME");
	}
	canvas->Update();

	parCanvas->cd();
	for (uint iPar=0; iPar<fitParGraphs.at(iCentBin).size(); iPar++){
	  parCanvas->cd(iPar+1);
	  fitParGraphs.at(iCentBin).at(iPar)->Draw("PZ");
	}
	parCanvas->Update();
	gSystem->Sleep(1000);

      }
      
      //Clean UP
      for (uint i=0; i<fitFuncResults.size(); i++){
	delete fitFuncResults.at(i);
      }

      
      //----------------------------------------------
      //Fit the Spectrum with Heinz Blast Wave Model for systematics
      enum blastWavePars {BW_TEMP,BW_SURFACEVELOCITY,BW_TRANSVELOCITYPOWER,BW_RADIALINTEGRALLIMIT,BW_PMASS,BW_AMP};
      std::vector<std::vector<int> > blastWaveParRelations (spectraToFit.at(iCentBin).at(yIndex).size(),
							    std::vector<int> (0));
      for (unsigned int i=0; i<spectraToFit.at(iCentBin).at(yIndex).size(); i++){
	blastWaveParRelations.at(i).push_back(BW_AMP+i);
	blastWaveParRelations.at(i).push_back(BW_TEMP);
	blastWaveParRelations.at(i).push_back(BW_SURFACEVELOCITY);
	blastWaveParRelations.at(i).push_back(BW_TRANSVELOCITYPOWER);
	blastWaveParRelations.at(i).push_back(BW_RADIALINTEGRALLIMIT);
	blastWaveParRelations.at(i).push_back(BW_PMASS);
      }

      std::vector<double> blastWaveTotalPars;
      blastWaveTotalPars.push_back(.1);
      blastWaveTotalPars.push_back(.68);
      blastWaveTotalPars.push_back(1);
      blastWaveTotalPars.push_back(1);
      blastWaveTotalPars.push_back(particleInfo->GetParticleMass(pid));
      for (unsigned int i=0; i<spectraToFit.at(iCentBin).at(yIndex).size(); i++)
	blastWaveTotalPars.push_back(10000);

      //Create the Sim Fitter
      SimFitter blastWaveFitter(spectraToFit.at(iCentBin).at(yIndex),blastWaveFitFuncs.at(iCentBin).at(yIndex));
      blastWaveFitter.Init(blastWaveTotalPars);
      blastWaveFitter.SetPrintLevel(0);
      blastWaveFitter.SetParameterRelationships(blastWaveParRelations);
      blastWaveFitter.FixParameter(BW_TRANSVELOCITYPOWER);
      blastWaveFitter.FixParameter(BW_RADIALINTEGRALLIMIT);
      blastWaveFitter.FixParameter(BW_PMASS);
      blastWaveFitter.SetParLimits(BW_TEMP,.08,.5);
      blastWaveFitter.SetParLimits(BW_SURFACEVELOCITY,.01,1);
      for (unsigned int i=0; i<spectraToFit.at(iCentBin).at(yIndex).size(); i++)
	blastWaveFitter.SetParLimits(BW_AMP+i,500,1000000000);

      ROOT::Fit::FitResult bwResults = blastWaveFitter.Fit();
      std::vector<TF1 *> bwFitFuncResults = blastWaveFitter.GetFitFunctions();
      cout <<"BW - Chi2/NDF: " <<bwResults.Chi2() <<" / " <<bwResults.Ndf() <<endl;

      for (unsigned int i=0; i<spectra.at(iCentBin).at(yIndex).size(); i++){
	bwFitFuncResults.at(i)->SetName(blastWaveFitFuncs.at(iCentBin).at(yIndex).at(i)->GetName());
	spectra.at(iCentBin).at(yIndex).at(i)->AddSpectraFit(bwFitFuncResults.at(i),
							     blastWaveFitter.GetCovarianceMatrix(i),1);
	cout <<"   dNdy: " <<spectra.at(iCentBin).at(yIndex).at(i)->GetdNdy(1) <<"+-"
	     <<spectra.at(iCentBin).at(yIndex).at(i)->GetdNdyErr(1) <<endl;
      }
      
      if (draw){
	canvas->cd();

	for (unsigned int iFunc=0; iFunc<bwFitFuncResults.size(); iFunc++){
	  bwFitFuncResults.at(iFunc)->SetLineColor(kBlue);
	  bwFitFuncResults.at(iFunc)->Draw("SAME");
	}
	canvas->Update();
      }

      //Clean Up
      for (unsigned int i=0; i<bwFitFuncResults.size(); i++){
	delete bwFitFuncResults.at(i);
      }
      
    }//End Loop Over Rapidity Bins

  }//End Loop Over Centrality Bins


  //SAVE SPECTRUM
  spectraFile->cd();
  TString saveDir = TString::Format("FitSpectra_%s",particleInfo->GetParticleName(pid,charge).Data());
  if (!gDirectory->GetKey(saveDir.Data()))
    spectraFile->mkdir(saveDir.Data());
  if (!gDirectory->GetKey(spectrumFitDir.Data()))
    spectraFile->mkdir(spectrumFitDir.Data());	  

  for (unsigned int iCentBin=0; iCentBin<nCentBins; iCentBin++){
    for (unsigned int yIndex=0; yIndex<nRapidityBins; yIndex++){
      
      for (uint i=0; i<spectra.at(iCentBin).at(yIndex).size(); i++){
	spectra.at(iCentBin).at(yIndex).at(i)->SetSpectraFile(spectraFile);
	spectra.at(iCentBin).at(yIndex).at(i)->SetSpectraClassDir(saveDir);
	spectra.at(iCentBin).at(yIndex).at(i)->SetSpectraFitDir(spectrumFitDir);
	
	//Save the Spectrum Class
	spectra.at(iCentBin).at(yIndex).at(i)->WriteSpectrum(false,true);
      }
    }//End Loop Over yIndex
  }//End Loop Over Centrality Bins
  

}
