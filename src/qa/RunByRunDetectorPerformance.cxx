//Create Run-By-Run Detector performance plots for qa purposes

#include <iostream>
#include <vector>
#include <utility>

#include <TString.h>
#include <TFile.h>
#include <TTree.h>
#include <TClonesArray.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TH3D.h>
#include <TMath.h>

#include "globalDefinitions.h"
#include "utilityFunctions.h"
#include "usefulDataStructs.h"
#include "TrackInfo.h"
#include "PrimaryVertexInfo.h"
#include "EventInfo.h"
#include "DavisDstReader.h"
#include "ParticleInfo.h"
#include "UserCuts.h"

//Defined Below_______________________________________________________
int FindRunNumberIndex(std::vector<int> *runNumbers, int runNumber);

//MAIN________________________________________________________________
void RunByRunDetectorPerformance(TString inputDataFile, TString outputFile=""){

  //Read the DavisDst
  DavisDstReader *davisDst = NULL;
  if (GetZVertexCuts().first != -999 && GetZVertexCuts().second != 999)
    davisDst = new DavisDstReader(inputDataFile,GetZVertexCuts().first,GetZVertexCuts().second);
  else
    davisDst = new DavisDstReader(inputDataFile);
  
  //Create Pointers needed for reading the tree
  TrackInfo *track = NULL;
  PrimaryVertexInfo *primaryVertex = NULL;
  EventInfo *event = NULL;
  
  //If no output file was specified then we won't produce one
  TFile *outFile = NULL;
  if (outputFile.CompareTo(""))
    outFile = new TFile(outputFile,"RECREATE");
  
  std::vector<unsigned int> allowedTriggers = GetAllowedTriggers();
  std::vector<int> runNumbers;
  std::vector<TH1D *> sectorIDHisto;
  std::vector<TH2D *> etaPhiHisto;
  
  //Loop Over the entries/events
  for (Long64_t iEntry=0; iEntry<davisDst->GetEntries(); iEntry++){

    event = davisDst->GetEntry(iEntry);
    if (!IsGoodEvent(event))
      continue;

    //Get the Run Number Index
    int runIDIndex=FindRunNumberIndex(&runNumbers,event->GetRunNumber());

    if (runIDIndex < 0){
      runNumbers.push_back(event->GetRunNumber());
      sectorIDHisto.push_back(new TH1D(Form("TPCSectorID_%d",event->GetRunNumber()),
				       Form("TPC Sector ID | RunID=%d;TPC Sector ID",event->GetRunNumber()),
				       24,.5,24.5));
      etaPhiHisto.push_back(new TH2D(Form("EtaPhiHisto_%d",event->GetRunNumber()),
				     Form("#eta Vs. #phi | RunID=%d;#eta;#phi",
					  event->GetRunNumber()),2,-1,1,12,-TMath::Pi(),TMath::Pi()));
      
      runIDIndex = runNumbers.size()-1;
    }

    //Loop Over the Vertices
    for (Int_t iVertex=0; iVertex<event->GetNPrimaryVertices(); iVertex++){

      primaryVertex = event->GetPrimaryVertex(iVertex);
      if (!IsGoodVertex(primaryVertex))
        continue;

      //Get the Centrality Bin
      UInt_t triggerID = event->GetTriggerID(&allowedTriggers);
      Int_t centralityVariable = GetCentralityVariable(primaryVertex);
      Int_t centralityBin      = GetCentralityBin(centralityVariable,triggerID,
                                                  primaryVertex->GetZVertex());

      //Skip events with bad centrality bins
      if (centralityBin < 0)
        continue;
      
      //Loop Over the Tracks
      for (Int_t iTrack=0; iTrack<primaryVertex->GetNPrimaryTracks(); iTrack++){
	
        track = primaryVertex->GetPrimaryTrack(iTrack);
        if (!IsGoodTrack(track))
          continue;

	//Fill the Track QA Plots
	sectorIDHisto.at(runIDIndex)->Fill(GetSectorID(track->GetEta(),track->GetPhi()));
	etaPhiHisto.at(runIDIndex)->Fill(track->GetEta(),track->GetPhi());
	
      }//End Loop Over Tracks

  
    }//End Loop Over Vertices

  }//End Loop Over Events

  
  //Save
  if (outFile){
    outFile->cd();

    outFile->mkdir("EtaPhiHistos");
    outFile->cd("EtaPhiHistos");
    for (unsigned int i=0; i<etaPhiHisto.size(); i++){
      if (etaPhiHisto.at(i)->GetEntries() > 0)
	etaPhiHisto.at(i)->Write();
      if (sectorIDHisto.at(i)->GetEntries() > 0)
	sectorIDHisto.at(i)->Write();
    }
  }

  outFile->Close();
  
}

//___________________________________________________________
int FindRunNumberIndex(std::vector<int> *runNumbers, int runNumber){

  //Return the index for this run number, if the run number is not found
  //then return -1

  for (unsigned int i=0; i<runNumbers->size(); i++){

    if (runNumbers->at(i) == runNumber)
      return i;
    
  }//End Loop Over Run Numbers

  return -1;
}

