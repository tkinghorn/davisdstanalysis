//This code uses the output of the background simulations to determine
//the fractional background of muons in the pion spectra.

#include <iostream>
#include <vector>
#include <utility>

#include <TCanvas.h>
#include <TString.h>
#include <TFile.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TH3D.h>
#include <TH2.h>
#include <TH3.h>
#include <TMath.h>
#include <TF1.h>
#include <TSystem.h>
#include <TPaveText.h>
#include <TLegend.h>
#include <TGraphAsymmErrors.h>
#include <TVirtualFitter.h>
#include <TDirectory.h>

#include "globalDefinitions.h"
#include "utilityFunctions.h"
#include "ParticleInfo.h"
#include "UserCuts.h"
#include "StyleSettings.h"

Bool_t draw=false;
Bool_t save=true;

//____MAIN___________________________________________________________
void muonContamination(TString inputFile, TString outputFile){

  //Create an instance of the ParticleInfo Class
  ParticleInfo *particleInfo = new ParticleInfo(GetStarLibraryVersion());

  //Get the Number of Centrality Bins
  const unsigned int nCentralityBins = GetNCentralityBins();

  //Open the inputFile and Load the Histograms
  TFile *inFile = new TFile(inputFile,"READ");
  std::vector < std::pair<TH2F *, TH2F *> > pionHisto
    (nCentralityBins,std::make_pair((TH2F *)NULL,(TH2F *)NULL));
  std::vector < std::pair<TH2F *, TH2F *> > muonHisto
    (nCentralityBins,std::make_pair((TH2F *)NULL,(TH2F *)NULL));
  for (unsigned int iCentBin=0; iCentBin<nCentralityBins; iCentBin++){

    pionHisto.at(iCentBin).first = (TH2F *)inFile->
      Get(Form("PionMuonContamination/pionCount_PionMinus_cent%02d",iCentBin));
    pionHisto.at(iCentBin).second = (TH2F *)inFile->
      Get(Form("PionMuonContamination/pionCount_PionPlus_cent%02d",iCentBin));

    muonHisto.at(iCentBin).first = (TH2F *)inFile->
      Get(Form("PionMuonContamination/muonCount_PionMinus_cent%02d",iCentBin));
    muonHisto.at(iCentBin).second = (TH2F *)inFile->
      Get(Form("PionMuonContamination/muonCount_PionPlus_cent%02d",iCentBin));

  }//End Loop Over Cent Bins to get the histograms from the file

  //Open the output file for writing
  TFile *outFile = new TFile(outputFile,save ? "UPDATE":"READ");
  outFile->cd();

  //Make the Graphs to hold the Background Fraction Plots and functions to fit them
  std::vector <std::vector< std::pair<TGraphAsymmErrors *, TGraphAsymmErrors *> > > muonBackground
    (nCentralityBins, std::vector< std::pair<TGraphAsymmErrors *, TGraphAsymmErrors *> >
     (nRapidityBins, std::make_pair((TGraphAsymmErrors *)NULL,(TGraphAsymmErrors *)NULL)));
  std::vector <std::vector< std::pair<TF1 *, TF1 *> > > muonBackgroundFit
    (nCentralityBins, std::vector< std::pair<TF1 *, TF1 *> >
     (nRapidityBins, std::make_pair((TF1 *)NULL,(TF1 *)NULL)));
  std::vector <std::vector< std::pair<TF1 *, TF1 *> > > muonBackgroundFit1
    (nCentralityBins, std::vector< std::pair<TF1 *, TF1 *> >
     (nRapidityBins, std::make_pair((TF1 *)NULL,(TF1 *)NULL)));
  std::vector <std::vector< std::pair<TGraphErrors *, TGraphErrors *> > > muonBackgroundFitSys
    (nCentralityBins, std::vector< std::pair<TGraphErrors *, TGraphErrors *> >
     (nRapidityBins, std::make_pair((TGraphErrors *)NULL,(TGraphErrors *)NULL)));
  std::vector <std::vector< std::pair<TGraphErrors *, TGraphErrors *> > > muonBackgroundFitSys1
    (nCentralityBins, std::vector< std::pair<TGraphErrors *, TGraphErrors *> >
     (nRapidityBins, std::make_pair((TGraphErrors *)NULL,(TGraphErrors *)NULL)));
  
  for (unsigned int iCentBin=0; iCentBin<nCentralityBins; iCentBin++){
    for (unsigned int yIndex=0; yIndex<nRapidityBins; yIndex++){

      //Background Graphs
      muonBackground.at(iCentBin).at(yIndex).first = new TGraphAsymmErrors();
      muonBackground.at(iCentBin).at(yIndex).second = new TGraphAsymmErrors();

      muonBackground.at(iCentBin).at(yIndex).first->
	SetName(Form("muonBackground_%s_cent%02d_yIndex%02d",
		     particleInfo->GetParticleName(PION,-1).Data(),iCentBin,yIndex));
      muonBackground.at(iCentBin).at(yIndex).first->
	SetTitle(Form("%s Contamination Fraction | Cent=[%d,%d]%% | y_{%s}^{Reco}=[%.2f,%.2f];(m_{T}-m_{%s})^{Reco} (GeV/c^{2}); #mu Contamination Fraction",
		      particleInfo->GetParticleSymbol(MUON,-1).Data(),
		      iCentBin!=nCentralityBins-1 ? (int)GetCentralityPercents().at(iCentBin+1):0,
		      (int)GetCentralityPercents().at(iCentBin),
		      particleInfo->GetParticleSymbol(PION,-1).Data(),
		      GetRapidityRangeLow(yIndex),GetRapidityRangeHigh(yIndex),
		      particleInfo->GetParticleSymbol(PION,-1).Data()));

      muonBackground.at(iCentBin).at(yIndex).second->
	SetName(Form("muonBackground_%s_cent%02d_yIndex%02d",
		     particleInfo->GetParticleName(PION,1).Data(),iCentBin,yIndex));
      muonBackground.at(iCentBin).at(yIndex).second->
	SetTitle(Form("%s Contamination Fraction | Cent=[%d,%d]%% | y_{%s}^{Reco}=[%.2f,%.2f];(m_{T}-m_{%s})^{Reco} (GeV/c^{2}); #mu Contamination Fraction",
		      particleInfo->GetParticleSymbol(MUON,1).Data(),
		      iCentBin!=nCentralityBins-1 ? (int)GetCentralityPercents().at(iCentBin+1):0,
		      (int)GetCentralityPercents().at(iCentBin),
		      particleInfo->GetParticleSymbol(PION,1).Data(),
		      GetRapidityRangeLow(yIndex),GetRapidityRangeHigh(yIndex),
		      particleInfo->GetParticleSymbol(PION,1).Data()));

      muonBackground.at(iCentBin).at(yIndex).first->SetMarkerColor(kBlack);
      muonBackground.at(iCentBin).at(yIndex).first->SetMarkerStyle(particleInfo->GetParticleMarker(PION,-1));

      muonBackground.at(iCentBin).at(yIndex).second->SetMarkerColor(kBlack);
      muonBackground.at(iCentBin).at(yIndex).second->SetMarkerStyle(particleInfo->GetParticleMarker(PION,1));

      //Background Fits
      muonBackgroundFit.at(iCentBin).at(yIndex).first =
	new TF1(Form("%s_FitExp",muonBackground.at(iCentBin).at(yIndex).first->GetName()),"[0]*TMath::Exp([1]*x)",.04,2);
      muonBackgroundFit.at(iCentBin).at(yIndex).second =
	new TF1(Form("%s_FitExp",muonBackground.at(iCentBin).at(yIndex).second->GetName()),"[0]*TMath::Exp([1]*x)",.04,2);
      muonBackgroundFit1.at(iCentBin).at(yIndex).first =
	new TF1(Form("%s_FitPower",muonBackground.at(iCentBin).at(yIndex).first->GetName()),"[0]*TMath::Power(x,[1])",.04,2);
      muonBackgroundFit1.at(iCentBin).at(yIndex).second =
	new TF1(Form("%s_FitPower",muonBackground.at(iCentBin).at(yIndex).second->GetName()),"[0]*TMath::Power(x,[1])",.04,2);

      muonBackgroundFit.at(iCentBin).at(yIndex).first->SetParameters(0.01,-1.5);
      muonBackgroundFit.at(iCentBin).at(yIndex).first->SetParLimits(0,0,0.2);
      muonBackgroundFit.at(iCentBin).at(yIndex).first->SetParLimits(1,-2.5,0);

      muonBackgroundFit.at(iCentBin).at(yIndex).second->SetParameters(0.01,-1.5);
      muonBackgroundFit.at(iCentBin).at(yIndex).second->SetParLimits(0,0,0.2);
      muonBackgroundFit.at(iCentBin).at(yIndex).second->SetParLimits(1,-2.5,0);

      muonBackgroundFit.at(iCentBin).at(yIndex).first->SetLineStyle(1);
      muonBackgroundFit.at(iCentBin).at(yIndex).first->SetLineWidth(3);
      muonBackgroundFit.at(iCentBin).at(yIndex).first->SetLineColor(kRed);

      muonBackgroundFit.at(iCentBin).at(yIndex).second->SetLineStyle(1);
      muonBackgroundFit.at(iCentBin).at(yIndex).second->SetLineWidth(3);
      muonBackgroundFit.at(iCentBin).at(yIndex).second->SetLineColor(kRed);

      
      muonBackgroundFit1.at(iCentBin).at(yIndex).first->SetParameters(0.02,-0.5);
      muonBackgroundFit1.at(iCentBin).at(yIndex).first->SetParLimits(0,0,0.5);
      muonBackgroundFit1.at(iCentBin).at(yIndex).first->SetParLimits(1,-2.5,0);

      muonBackgroundFit1.at(iCentBin).at(yIndex).second->SetParameters(0.02,-0.5);
      muonBackgroundFit1.at(iCentBin).at(yIndex).second->SetParLimits(0,0,0.5);
      muonBackgroundFit1.at(iCentBin).at(yIndex).second->SetParLimits(1,-2.5,0);

      muonBackgroundFit1.at(iCentBin).at(yIndex).first->SetLineStyle(9);
      muonBackgroundFit1.at(iCentBin).at(yIndex).first->SetLineWidth(3);
      muonBackgroundFit1.at(iCentBin).at(yIndex).first->SetLineColor(kBlue);

      muonBackgroundFit1.at(iCentBin).at(yIndex).second->SetLineStyle(9);
      muonBackgroundFit1.at(iCentBin).at(yIndex).second->SetLineWidth(3);
      muonBackgroundFit1.at(iCentBin).at(yIndex).second->SetLineColor(kBlue);

    }//End Loop Over Rapidity Bins
  }//End Loop Over Centraltiy bins to create the background graphs
  
  //Compute the Muon Contamination Background Fraction
  for (unsigned int iCentBin=0; iCentBin<nCentralityBins; iCentBin++){

    for (int iBinX=1; iBinX<=pionHisto.at(iCentBin).first->GetNbinsX(); iBinX++){

      //Get the Rapidity Bin Index
      int yIndex = GetRapidityIndex(pionHisto.at(iCentBin).first->GetXaxis()->GetBinCenter(iBinX));

      //Keep Track of the minimum mTm0 of the graph (see below)
      Double_t minmTm0(0);

      //Negative
      muonBackground.at(iCentBin).at(yIndex).first->
	BayesDivide(muonHisto.at(iCentBin).first->ProjectionY("muonminus",iBinX,iBinX)->Rebin(2),
		    pionHisto.at(iCentBin).first->ProjectionY("pionminus",iBinX,iBinX)->Rebin(2));

      TGraphChop((TGraphErrors *)muonBackground.at(iCentBin).at(yIndex).first,.05,true);
      TGraphChop((TGraphErrors *)muonBackground.at(iCentBin).at(yIndex).first,1.0,false);

      //Fit and Confidence Interval
      if (muonBackground.at(iCentBin).at(yIndex).first->GetN() > 2){

	//Only Fit in the Range where we have data points
	minmTm0 = muonBackground.at(iCentBin).at(yIndex).first->GetX()[0] -
	  muonBackground.at(iCentBin).at(yIndex).first->GetEXlow()[0];
	muonBackgroundFit.at(iCentBin).at(yIndex).first->SetRange(minmTm0,2);
	muonBackgroundFit1.at(iCentBin).at(yIndex).first->SetRange(minmTm0,2);
	
	muonBackground.at(iCentBin).at(yIndex).first->Fit(muonBackgroundFit.at(iCentBin).at(yIndex).first,"EX0R");
	muonBackgroundFitSys.at(iCentBin).at(yIndex).first =
	  GetConfidenceIntervalOfFit(muonBackgroundFit.at(iCentBin).at(yIndex).first);
	
	muonBackground.at(iCentBin).at(yIndex).first->Fit(muonBackgroundFit1.at(iCentBin).at(yIndex).first,"EX0R+");
	muonBackgroundFitSys1.at(iCentBin).at(yIndex).first =
	  GetConfidenceIntervalOfFit(muonBackgroundFit1.at(iCentBin).at(yIndex).first);
      }
      else {
	delete muonBackground.at(iCentBin).at(yIndex).first;
	delete muonBackgroundFit.at(iCentBin).at(yIndex).first;
	delete muonBackgroundFitSys.at(iCentBin).at(yIndex).first;

	muonBackground.at(iCentBin).at(yIndex).first = NULL;
	muonBackgroundFit.at(iCentBin).at(yIndex).first = NULL;
	muonBackgroundFitSys.at(iCentBin).at(yIndex).first = NULL;
      }
      
      //Positive
      muonBackground.at(iCentBin).at(yIndex).second->
	BayesDivide(muonHisto.at(iCentBin).second->ProjectionY("muonplus",iBinX,iBinX)->Rebin(2),
		    pionHisto.at(iCentBin).second->ProjectionY("pionplus",iBinX,iBinX)->Rebin(2));
      
      TGraphChop((TGraphErrors *)muonBackground.at(iCentBin).at(yIndex).second,.05,true);
      TGraphChop((TGraphErrors *)muonBackground.at(iCentBin).at(yIndex).second,1.0,false);

      //Fit and Confidience Interval
      if (muonBackground.at(iCentBin).at(yIndex).second->GetN() > 2){
	
	//Only Fit in the Range where we have data points
	minmTm0 = muonBackground.at(iCentBin).at(yIndex).second->GetX()[0] -
	  muonBackground.at(iCentBin).at(yIndex).second->GetEXlow()[0];
	muonBackgroundFit.at(iCentBin).at(yIndex).second->SetRange(minmTm0,2);
	muonBackgroundFit1.at(iCentBin).at(yIndex).second->SetRange(minmTm0,2);
	
	muonBackground.at(iCentBin).at(yIndex).second->Fit(muonBackgroundFit.at(iCentBin).at(yIndex).second,"EX0R");
	muonBackgroundFitSys.at(iCentBin).at(yIndex).second =
	  GetConfidenceIntervalOfFit(muonBackgroundFit.at(iCentBin).at(yIndex).second);
	
	muonBackground.at(iCentBin).at(yIndex).second->Fit(muonBackgroundFit1.at(iCentBin).at(yIndex).second,"EX0R+");
	muonBackgroundFitSys1.at(iCentBin).at(yIndex).second =
	  GetConfidenceIntervalOfFit(muonBackgroundFit1.at(iCentBin).at(yIndex).second);
      }
      else {
	delete muonBackground.at(iCentBin).at(yIndex).second;
	delete muonBackgroundFit.at(iCentBin).at(yIndex).second;
	delete muonBackgroundFitSys.at(iCentBin).at(yIndex).second;
	
	muonBackground.at(iCentBin).at(yIndex).second = NULL;
	muonBackgroundFit.at(iCentBin).at(yIndex).second = NULL;
	muonBackgroundFitSys.at(iCentBin).at(yIndex).second = NULL;
	
      }

    }//End Loop Over Xaxis (rapidity Axis)

    
  }//End Loop Over centrality bins to compute background


  //Make a canvas
  TCanvas *canvas = NULL;
  if (draw){
    canvas = new TCanvas("canvas","canvas",20,20,800,600);
    canvas->SetTopMargin(.05);
    canvas->SetRightMargin(.05);
    canvas->SetTicks();
    muonBackground.at(8).at(20).second->Draw("APZ");
    muonBackgroundFitSys.at(8).at(20).second->Draw("3");
  }

  //Make the Directories if necessary
  outFile->cd();
  outFile->mkdir("PionMinus");
  outFile->mkdir("PionPlus");
  
  //Save 
  outFile->cd();
  outFile->cd("PionMinus");
  gDirectory->mkdir("MuonBackgroundGraphs");
  gDirectory->mkdir("MuonBackgroundFits");

  outFile->cd();
  outFile->cd("PionPlus");
  gDirectory->mkdir("MuonBackgroundGraphs");
  gDirectory->mkdir("MuonBackgroundFits");

  for (unsigned int iCentBin=0; iCentBin<nCentralityBins; iCentBin++){
    for (unsigned int yIndex=0; yIndex<nRapidityBins; yIndex++){
      
      outFile->cd();
      outFile->cd(Form("%s/MuonBackgroundGraphs",particleInfo->GetParticleName(PION,-1).Data()));
      if (muonBackground.at(iCentBin).at(yIndex).first){
	muonBackground.at(iCentBin).at(yIndex).first->
	  Write(muonBackground.at(iCentBin).at(yIndex).first->GetName(),TObject::kOverwrite);
      }
      else {
	continue;
      }
      
      outFile->cd();
      outFile->cd(Form("%s/MuonBackgroundFits",particleInfo->GetParticleName(PION,-1).Data()));
      if (muonBackgroundFit.at(iCentBin).at(yIndex).first){
	muonBackgroundFit.at(iCentBin).at(yIndex).first->
	  Write(muonBackgroundFit.at(iCentBin).at(yIndex).first->GetName(),TObject::kOverwrite);
	muonBackgroundFitSys.at(iCentBin).at(yIndex).first->
	  Write(muonBackgroundFitSys.at(iCentBin).at(yIndex).first->GetName(),TObject::kOverwrite);
	
	muonBackgroundFit1.at(iCentBin).at(yIndex).first->
	  Write(muonBackgroundFit1.at(iCentBin).at(yIndex).first->GetName(),TObject::kOverwrite);
	muonBackgroundFitSys1.at(iCentBin).at(yIndex).first->
	  Write(muonBackgroundFitSys1.at(iCentBin).at(yIndex).first->GetName(),TObject::kOverwrite);
      }
      
      outFile->cd();
      outFile->cd(Form("%s/MuonBackgroundGraphs",particleInfo->GetParticleName(PION,1).Data()));
      if (muonBackground.at(iCentBin).at(yIndex).second){
	muonBackground.at(iCentBin).at(yIndex).second->
	  Write(muonBackground.at(iCentBin).at(yIndex).second->GetName(),TObject::kOverwrite);
      }
      else {
	continue;
      }
      
      outFile->cd();
      outFile->cd(Form("%s/MuonBackgroundFits",particleInfo->GetParticleName(PION,1).Data()));
      if (muonBackgroundFit.at(iCentBin).at(yIndex).second){
	muonBackgroundFit.at(iCentBin).at(yIndex).second->
	  Write(muonBackgroundFit.at(iCentBin).at(yIndex).second->GetName(),TObject::kOverwrite);
	muonBackgroundFitSys.at(iCentBin).at(yIndex).second->
	  Write(muonBackgroundFitSys.at(iCentBin).at(yIndex).second->GetName(),TObject::kOverwrite);

	muonBackgroundFit1.at(iCentBin).at(yIndex).second->
	  Write(muonBackgroundFit1.at(iCentBin).at(yIndex).second->GetName(),TObject::kOverwrite);
	muonBackgroundFitSys1.at(iCentBin).at(yIndex).second->
	  Write(muonBackgroundFitSys1.at(iCentBin).at(yIndex).second->GetName(),TObject::kOverwrite);
      }
      
    }//End Loop Over yIndex
  }//End Loop Over centIndex
  
}
